<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Command\Download;

use AppBundle\Entity\Flux\Download;
use AppBundle\Entity\Flux\Process;
use AppBundle\Entity\User\User;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CleanCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('datatourisme:download:clean')
            ->setDescription('Batch command to clean download stats');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var EntityManagerInterface $em */
        $em = $this->getContainer()->get('doctrine')->getManager();
        $conn = $em->getConnection();

        $date = new \DateTime();
        $date->modify('-1 year');

        $file = $this->getContainer()->get('kernel')->getProjectDir() . '/var/archives/download.tsv.gz';
        $exists = file_exists($file);
        @mkdir(dirname($file), 0755, true);

        $sql = "SELECT * FROM download WHERE created_at < :date";
        $handle = gzopen($file, "a");
        $stmt = $conn->executeQuery($sql, ['date' => $date->format('Y-m-d')]);
        while ($row = $stmt->fetch()) {
            if (!$exists) {
                fwrite($handle, join("\t", array_keys($row) . "\n"));
                $exists = true;
            }
            fwrite($handle, join("\t", array_values($row) . "\n"));
        }
        fclose($handle);
        
        $sql = "DELETE FROM download WHERE created_at < :date";
        $countDeleted = $conn->executeUpdate($sql, ['date' => $date->format('Y-m-d')]);

        $output->writeln("$countDeleted deleted entries");
    }
}
