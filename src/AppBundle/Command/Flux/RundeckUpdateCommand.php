<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Command\Flux;

use AppBundle\Entity\Flux\Flux;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class RundeckUpdateCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('datatourisme:flux:rundeck:update')

            // the short description shown while running "php bin/console list"
            ->setDescription('Update all scheduled rundeck job.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();
        $repository = $em->getRepository(Flux::class);

        $fluxList = $repository->findAll();

        /** @var Flux $flux */
        foreach ($fluxList as $flux) {
            if (null != $flux->getScheduleHour()) {
                $this->getContainer()->get('app.process_cache')->updateRundeckCron($flux);
                $output->writeln("<info>Définition du job Rundeck du flux {$flux->getId()} mise à jour.</info>");
            }
        }
    }
}
