<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Command\Process;

use AppBundle\Entity\Flux\Process;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CheckCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('datatourisme:process:check')

            // the short description shown while running "php bin/console list"
            ->setDescription('Check all running task over beanstalkd.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();
        $repository = $em->getRepository(Process::class);
        $bsManager = $this->getContainer()->get('webapp.beanstalk.messenger');
        //$sm = $this->getContainer()->get('state_machine.process_operation');

        $processes = $repository->findBy(array('status' => array(Process::STATUS_RUNNING)));
        /** @var Process $process */
        foreach ($processes as $process) {
            $jobId = $process->getLastJob();
            if ($jobId) {
                $isAlive = $bsManager->isAlive($jobId);
                if (!$isAlive) {
                    $process->setStatus(Process::STATUS_ERROR);
                    $output->writeln("<error>Le traitement {$process->getId()} n'est plus actif et a été mis en erreur.</error>");
                } else {
                    $output->writeln("<info>Le traitement {$process->getId()} est toujours actif.</info>");
                }
            } else {
                // todo : job id null
            }
        }
        $em->flush();
        // ...
    }
}
