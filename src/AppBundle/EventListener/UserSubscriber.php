<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\EventListener;

use AppBundle\Entity\Flux\Flux;
use AppBundle\Entity\User\User;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

/**
 * Class UserSubscriber.
 */
class UserSubscriber implements EventSubscriber
{
    /**
     * @var AuthorizationChecker
     */
    protected $authorizationChecker;

    /**
     * @var RequestStack
     */
    protected $requestStack;

    /**
     * UserSubscriber constructor.
     *
     * @param AuthorizationChecker $authorizationChecker
     */
    public function __construct(AuthorizationChecker $authorizationChecker, RequestStack $requestStack)
    {
        $this->authorizationChecker = $authorizationChecker;
        $this->requestStack = $requestStack;
    }

    /**
     * @return array
     */
    public function getSubscribedEvents()
    {
        return array(
            'prePersist',
            'postUpdate',
        );
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function prePersist(LifecycleEventArgs $args)
    {
        $user = $args->getObject();
        if ($user instanceof User) {
            if (empty($user->getLocale())) {
                $user->setLocale($this->requestStack->getCurrentRequest()->getLocale());
            }
        }
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function postUpdate(LifecycleEventArgs $args)
    {
        $user = $args->getObject();
        if ($user instanceof User) {
            $changeSet = array_keys($args->getEntityManager()->getUnitOfWork()->getEntityChangeSet($user));
            $this->roleUpdated($user, $changeSet, $args->getEntityManager());
            $this->apiKeyUpdated($user, $changeSet, $args->getEntityManager());
        }
    }

    /**
     * @param User          $user
     * @param array         $changeSet
     * @param EntityManager $em
     */
    protected function roleUpdated(User $user, $changeSet, EntityManager $em)
    {
        $roleHasChanged = in_array('role', $changeSet);
        if ($roleHasChanged) {
            $canPublishOnDatagouv = $this->authorizationChecker->isGranted('user.datagouv', $user);
            if (!$canPublishOnDatagouv) {
                $user->setDataGouvApiKey(null);
                $this->disableFluxDatagouv($user, $em);
            }
        }
    }

    /**
     * @param User          $user
     * @param array         $changeSet
     * @param EntityManager $em
     */
    protected function apiKeyUpdated(User $user, $changeSet, EntityManager $em)
    {
        $hasDatagouvApiKeyChanged = in_array('dataGouvApiKey', $changeSet);
        if ($hasDatagouvApiKeyChanged) {
            $this->disableFluxDatagouv($user, $em);
            $em->flush();
        }
    }

    /**
     * @param User          $user
     * @param EntityManager $em
     */
    protected function disableFluxDatagouv(User $user, EntityManager $em)
    {
        $userFlux = $em->getRepository(Flux::class)->findBy(array(
            'user' => $user,
            'dataGouv' => true,
        ));
        if (count($userFlux)) {
            foreach ($userFlux as $flux) {
                $flux->setDataGouv(false);
                $flux->setDataSet(null);
            }
            $em->flush();
        }
    }
}
