<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Notification\Type;

use AppBundle\Entity\Flux\Process;
use Datatourisme\Bundle\WebAppBundle\Notification\Type\AbstractTranslatableType;
use Monolog\Logger;

/**
 * Class ProcessCompleteType.
 */
class ProcessCompleteType extends AbstractTranslatableType
{
    /**
     * @return string
     */
    public function getMessage()
    {
        /** @var Process $process */
        $process = $this->getSubject();
        $key = Process::TYPE_PARTIAL == $process->getType() ? 'msg.process_done_partial' : 'msg.process_done_complete';

        return '{% trans with {"%name%": flux.name} from "emails" %}'.$key.'{% endtrans %}';
    }

    /**
     * @return int
     */
    public function getLevel()
    {
        return Logger::INFO;
    }

    /**
     * @return array
     */
    public function getContext()
    {
        return array('flux' => $this->getSubject()->getFlux());
    }

    /**
     * @return string
     */
    public function getRoute()
    {
        /** @var Process $process */
        $process = $this->getSubject();
        switch ($process->getType()) {
            case Process::TYPE_PARTIAL:
                return 'flux.editor';
                break;
            case Process::TYPE_COMPLETE:
                return 'flux.processes';
                break;
        }
    }

    /**
     * @return array
     */
    public function getRouteParameters()
    {
        return array('id' => $this->getSubject()->getFlux()->getId());
    }

    /**
     * @return mixed
     */
    public function getRecipients()
    {
        return $this->getSubject()->getFlux()->getUser();
    }
}
