<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Notification\Type;

use Datatourisme\Bundle\WebAppBundle\Notification\Type\AbstractTranslatableType;
use Monolog\Logger;

/**
 * Class ProcessCompleteType.
 */
class FluxDeprecatedType extends AbstractTranslatableType
{
    /**
     * @return string
     */
    public function getMessage()
    {
        return '{% trans with {"%name%": flux.name} from "emails" %}msg.deprecated_flux{% endtrans %}';
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return '<p>{% trans from "emails" %}text.deprecated_flux{% endtrans %}</p>';
    }

    /**
     * @return int
     */
    public function getLevel()
    {
        return Logger::INFO;
    }

    /**
     * @return array
     */
    public function getContext()
    {
        return array('flux' => $this->getSubject());
    }

    /**
     * @return string
     */
    public function getRoute()
    {
        return 'flux.parameters';
    }

    /**
     * @return array
     */
    public function getRouteParameters()
    {
        return array('id' => $this->getSubject()->getId());
    }

    /**
     * @return mixed
     */
    public function getRecipients()
    {
        return $this->getSubject()->getUser();
    }
}
