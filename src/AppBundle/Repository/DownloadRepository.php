<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Repository;

use AppBundle\Entity\Application\ApplicationType;
use AppBundle\Entity\Flux\Flux;
use AppBundle\Entity\Application\Application;
use AppBundle\Entity\User\User;
use AppBundle\Entity\Flux\OutputType;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;

/**
 * Class DownloadRepository.
 */
class DownloadRepository extends EntityRepository
{
    /**
     * @param $start
     * @param $end
     *
     * @return mixed
     */
    public function findBetweenDateRange($start, $end)
    {
        $qb = $this->createQueryBuilder('d')
            ->leftJoin(Flux::class, 'f', Join::WITH, 'd.flux = f')
            ->leftJoin(OutputType::class, 'ot', Join::WITH, 'd.outputType = ot')
            ->leftJoin(User::class, 'u', Join::WITH, 'f.user = u')
            ->leftJoin(Application::class, 'a', Join::WITH, 'd.application = a')
            ->leftJoin(ApplicationType::class, 'at', Join::WITH, 'a.type = at')
            ->addGroupBy('d.id')
            ->addGroupBy('f.id')
            ->addGroupBy('ot.id')
            ->addGroupBy('u.id')
            ->addGroupBy('a.id')
            ->addGroupBy('at.id')
            ->orderBy('d.createdAt', 'DESC');

        // after start date
        if ($start) {
            $qb->andWhere('d.createdAt >= :start')
                ->setParameter('start', $start);
        }

        // before end date
        if ($end) {
            $end->modify('+23 hours');
            $end->modify('+59 minutes');
            $end->modify('+59 seconds');
            $qb->andWhere('d.createdAt <= :end')
                ->setParameter('end', $end);
        }

        return $qb->getQuery()->execute();
    }
}
