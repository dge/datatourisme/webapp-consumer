<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Repository;

use AppBundle\Entity\Flux\Flux;
use AppBundle\Entity\Flux\OutputType;
use AppBundle\Entity\User\User;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

/**
 * Class FluxRepository.
 */
class FluxRepository extends EntityRepository
{
    /** @var TokenStorageInterface */
    private $tokenStorage;

    /** @var AuthorizationChecker */
    private $authorizationChecker;

    /**
     * @param TokenStorageInterface $tokenStorage
     */
    public function setTokenStorage(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param AuthorizationChecker $authorizationChecker
     */
    public function setAuthorizationChecker($authorizationChecker)
    {
        $this->authorizationChecker = $authorizationChecker;
    }

    /**
     * @return QueryBuilder
     */
    public function getIndexQueryBuilder()
    {
        /** @var QueryBuilder $qb */
        $qb = $this->createQueryBuilder('f')
            ->leftJoin(OutputType::class, 'o', Join::WITH, 'f.outputType = o')
            ->groupBy('f.id')
            ->addGroupBy('o.id');

        // filter by user if not admin or join the user
        $userIsAdmin = $this->authorizationChecker->isGranted('ROLE_ADMIN', $this->tokenStorage->getToken()->getUser());
        if (!$userIsAdmin) {
            $qb->where('f.user = :user')
                ->setParameter('user', $this->tokenStorage->getToken()->getUser());
        } else {
            $qb->leftJoin('AppBundle:User\User', 'u', Join::WITH, 'u = f.user')
                ->addGroupBy('u.id');
        }

        return $qb;
    }

    /**
     * Gets Flux count.
     *
     * @param bool $getQb       If true, returns QueryBuilder instead of result
     * @param bool $restrictive If true, restricts result to token's user's role
     *
     * @return QueryBuilder|int
     */
    public function getCount(bool $getQb = false, bool $restrictive = true)
    {
        $qb = $this->createQueryBuilder('flux');
        $qb->select('COUNT(flux.id)');

        if (true === $restrictive) {
            if (false === $this->authorizationChecker->isGranted('ROLE_ADMIN')) {
                $qb->where('flux.user = :user');
                $qb->setParameter('user', $this->tokenStorage->getToken()->getUser());
            }
        }

        return false === $getQb
            ? (int) $qb->getQuery()->getSingleScalarResult()
            : $qb
        ;
    }
}
