<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Repository;

use AppBundle\Entity\User\UserType;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;

/**
 * Class UserRepository.
 */
class UserRepository extends \Datatourisme\Bundle\WebAppBundle\Repository\UserRepository
{
    /**
     * @return QueryBuilder
     */
    public function getIndexQueryBuilder()
    {
        return $this->createQueryBuilder('u')
            ->leftJoin(UserType::class, 't', Join::WITH, 't = u.type')
            ->where('u.accountNonExpired = true')
            ->groupBy('u.id') // ?
            ->addGroupBy('t.id'); // ?
    }
}
