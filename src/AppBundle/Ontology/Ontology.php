<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Ontology;

/**
 * Instance of ontology
 */
class Ontology
{
    private $context;
    private $classes;
    private $properties;
    private $individuals;

    /**
     * constructor.
     *
     * @param array $ontology
     */
    public function __construct($ontology)
    {
        $this->context = $ontology['@context'];
        $this->classes = $ontology['classes'];
        $this->properties = $ontology['properties'];
        $this->individuals = $ontology['individuals'];
    }

    /**
     * Expand an uri
     */
    public function expand($uri) {
        if (is_array($uri)) {
            return array_map(function($u) { return $this->expand($u); }, $uri);
        }
        $parts = explode(":", $uri);
        if (isset($this->context[$parts[0]] )) {
            return $this->context[$parts[0]] . $parts[1];
        }
        return $uri;
    }

    /**
     * Get subclasses
     */
    public function getSubClasses($uri, $direct = true) {
        $resources = [];
        foreach ($this->classes as $key => $class) {
            if (isset($class['superClasses']) && in_array($uri, $class['superClasses'])) {
                $resources[$key] = $class;
                if (!$direct) {
                    $resources = array_merge($resources, $this->getSubClasses($class['uri'], false));
                }
			}
        }
        return $resources;
    }

    /**
     * Get individuals of a given type
     */
    public function getIndividuals($uri) {
        return array_filter($this->individuals, function($individual) use ($uri) {
			if (isset($individual['types'])) {
				return in_array($uri, $individual['types']);
			}
			return false;
		});
    }

    /**
     * Get resource
     */
    public function getResource($uri) {
        if (isset($this->classes[$uri])) {
            return $this->classes[$uri];
        } else if (isset($this->properties[$uri])) {
            return $this->properties[$uri];
        } else if (isset($this->individuals[$uri])) {
            return $this->individuals[$uri];
        }
        return null;        
    }

    /**
     * Get label
     */
    public function getLabel($uri) {
        $res = $this->getResource($uri);
        if ($res && isset($res['label'])) {
            return $res['label'];
        }
        return null;    
    }

    /**
     * Get property
     */
    public function getProperty($uri, $property) {
        $res = $this->getResource($uri);
        if ($res && isset($res['properties'][$property])) {
            return $res['properties'][$property];
        }
        return null;
    }
}
