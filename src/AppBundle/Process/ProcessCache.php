<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Process;

use AppBundle\Entity\Flux\Flux;
use AppBundle\Entity\Flux\Process;
use Datatourisme\Bundle\WebAppBundle\TaskManager\TaskManager;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\Router;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

/**
 * Created by PhpStorm.
 * User: nawer
 * Date: 15/09/17
 * Time: 15:22.
 */
class ProcessCache
{
    private $em;
    private $taskManager;
    private $authorizationChecker;
    private $router;

    public function __construct(ObjectManager $em, TaskManager $taskManager, AuthorizationChecker $authorizationChecker, Router $router)
    {
        $this->em = $em;
        $this->taskManager = $taskManager;
        $this->authorizationChecker = $authorizationChecker;
        $this->router = $router;
    }

    /**
     * @param Flux $flux
     * @param $type
     */
    public function generateFluxCacheData(Flux $flux, $type)
    {
        // create entity
        $process = new Process();
        $process->setType($type);
        $process->setFlux($flux);
        $process->setStatus(Process::STATUS_WAIT);
        $process->setOutputType($flux->getOutputType());
        $process->setChecksum($flux->getChecksum());

        $this->em->persist($process);
        $this->em->flush();

        // launch beanstalk task
        $taskName = 'consumer';
        $taskArguments = array(
            'uuid' => $process->getId(),
            'flux' => $flux->getId(),
            'languages' => $flux->getLanguages(),
            'query' => $flux->getQuery(),
            'format' => $flux->getOutputType()->getCode(),
            'partial' => Process::TYPE_PARTIAL == $type,
        );
        if (Process::TYPE_COMPLETE == $type && $this->authorizationChecker->isGranted('flux.datagouv', $flux)) {
            $taskArguments['datagouv'] = [
                'api' => $flux->getUser()->getDataGouvApiKey(),
                'dataset' => $flux->getDataSet(),
            ];
        }

        $this->taskManager->run($taskName, $taskArguments);
        $this->em->flush();
    }

    /**
     * Update or add rundeck task for given flux.
     *
     * @param Flux $flux
     *
     * @return bool
     */
    public function updateRundeckCron(Flux $flux)
    {
        // update or add cron task
        $this->taskManager->update(
            $flux->getId(),
            'cache-generation',
            $this->router->generate('internal.api.run', array('id' => $flux->getId()), UrlGeneratorInterface::ABSOLUTE_URL),
            $flux->getScheduleHour(),
            null,
            null !== $flux->getScheduleHour()
        );
    }
}
