<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Process\Report\TaskMessage;

use AppBundle\Entity\Flux\Process;
use AppBundle\Entity\RdfResource;
use AppBundle\Process\Report\JobMessage\ReportEntry;

class ProcessQueryResultMessage extends AbstractTaskMessage
{
    public function process(ReportEntry $report)
    {
        if ($report->getData()) {
            $data = $report->getData();
            if (Process::TYPE_COMPLETE == $this->process->getType()) {
                // delete all resources
                $this->em->createQueryBuilder()->delete(RdfResource::class, 'r')
                    ->where('r.flux = :flux')
                    ->setParameter('flux', $this->process->getFlux())
                    ->getQuery()->execute();

                if (isset($data['resources'])) {
                    // add resources
                    $this->batchProcess($data['resources'], function ($uri) {
                        $rdfResource = new RdfResource();
                        $rdfResource->setFlux($this->process->getFlux());
                        $rdfResource->setUri($uri);
                        $this->em->persist($rdfResource);
                    });
                }
            }
            // create process
            $this->process->setNbrPOI(isset($data['resources']) ? count($data['resources']) : 0);
            $this->process->setSize($data['size']);
        }

        return $this->process;
    }
}
