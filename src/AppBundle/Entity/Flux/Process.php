<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Entity\Flux;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model\Timestampable\Timestampable;

/**
 * @ORM\Table(name="process")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProcessRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Process
{
    use Timestampable;

    const TYPE_PARTIAL = 'partial';
    const TYPE_COMPLETE = 'complete';

    const STATUS_WAIT = 'wait';
    const STATUS_RUNNING = 'running';
    const STATUS_OK = 'ok';
    const STATUS_ERROR = 'error';

    public static $statusDefinitions = array(
        self::STATUS_WAIT => [
            'label.status.wait',
            'text.status.wait',
        ],
        self::STATUS_RUNNING => [
            'label.status.running',
            'text.status.running',
        ],
        self::STATUS_OK => [
            'label.status.ok',
            'text.status.ok',
        ],
        self::STATUS_ERROR => [
            'label.status.error',
            'text.status.error',
        ],
    );

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Flux
     * @ORM\ManyToOne(targetEntity="Flux", inversedBy="processes")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $flux;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $type;

    /**
     * @var string
     * @ORM\Column(type="string", length=15, nullable=false)
     */
    private $status;

    /**
     * @var OutputType
     * @ORM\ManyToOne(targetEntity="OutputType")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    protected $outputType;

    /**
     * @var int
     * @ORM\Column(type="bigint", nullable=true)
     */
    protected $nbrPOI;

    /**
     * @var int
     * @ORM\Column(type="bigint", nullable=true)
     */
    protected $size;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $checksum;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=true)
     */
    private $lastJob;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Log", mappedBy="process")
     */
    private $logs;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \AppBundle\Entity\Flux\Flux
     */
    public function getFlux()
    {
        return $this->flux;
    }

    /**
     * @param \AppBundle\Entity\Flux\Flux $flux
     */
    public function setFlux($flux)
    {
        $this->flux = $flux;
    }

    /**
     * @param OutputType $outputType
     *
     * @return Process
     */
    public function setOutputType($outputType)
    {
        $this->outputType = $outputType;

        return $this;
    }

    /**
     * @return OutputType
     */
    public function getOutputType()
    {
        return $this->outputType;
    }

    /**
     * @return int
     */
    public function getNbrPOI()
    {
        return $this->nbrPOI;
    }

    /**
     * @param int $nbrPOI
     */
    public function setNbrPOI($nbrPOI)
    {
        $this->nbrPOI = $nbrPOI;
    }

    /**
     * @return int
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param int $size
     */
    public function setSize($size)
    {
        $this->size = $size;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return int
     */
    public function getChecksum()
    {
        return $this->checksum;
    }

    /**
     * @param int $checksum
     */
    public function setChecksum($checksum)
    {
        $this->checksum = $checksum;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return int
     */
    public function getLastJob()
    {
        return $this->lastJob;
    }

    /**
     * @param int $lastJob
     *
     * @return Process
     */
    public function setLastJob($lastJob)
    {
        $this->lastJob = $lastJob;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getLogs()
    {
        return $this->logs;
    }

    /**
     * @param ArrayCollection $logs
     */
    public function setLogs($logs)
    {
        $this->logs = $logs;
    }

    /**
     * Return true if the flux is currently waiting or running.
     *
     * @return bool
     */
    public function isRunning()
    {
        return in_array($this->getStatus(), [self::STATUS_WAIT, self::STATUS_RUNNING]);
    }

    /**
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        if (self::TYPE_COMPLETE == $this->getType()) {
            $this->getFlux()->setLastProcessAt(time());
        }
    }

    /**
     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        if (self::TYPE_COMPLETE == $this->getType()) {
            $this->getFlux()->setLastProcessAt(time());
        }
    }
}
