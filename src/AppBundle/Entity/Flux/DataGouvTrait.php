<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Entity\Flux;

use Doctrine\ORM\Mapping as ORM;

trait DataGouvTrait
{
    /**
     * @var bool
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $dataGouv;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $dataSet;

    /**
     * @return bool
     */
    public function isDataGouv()
    {
        return $this->dataGouv;
    }

    /**
     * @param bool $dataGouv
     */
    public function setDataGouv($dataGouv)
    {
        $this->dataGouv = $dataGouv;
    }

    /**
     * @return string
     */
    public function getDataSet()
    {
        return $this->dataSet;
    }

    /**
     * @param string $dataSet
     */
    public function setDataSet($dataSet)
    {
        $this->dataSet = $dataSet;
    }
}
