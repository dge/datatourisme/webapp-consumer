<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Entity;

use AppBundle\Entity\Flux\Flux;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name = "rdf_resource")
 * @ORM\Entity()
 */
class RdfResource
{
    /**
     * @ORM\Column(type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=511, nullable=false)
     * @Assert\NotBlank()
     * @Assert\Length(max=511)
     */
    private $uri;

    /**
     * @var Flux
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Flux\Flux", inversedBy="rdfResources")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $flux;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getUri()
    {
        return $this->uri;
    }

    /**
     * @param string $uri
     */
    public function setUri(string $uri)
    {
        $this->uri = $uri;
    }

    /**
     * @return Flux
     */
    public function getFlux()
    {
        return $this->flux;
    }

    /**
     * @param Flux $flux
     */
    public function setFlux(Flux $flux)
    {
        $this->flux = $flux;
    }
}
