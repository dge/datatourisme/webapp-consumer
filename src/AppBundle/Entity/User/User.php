<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Entity\User;

use Datatourisme\Bundle\WebAppBundle\Mailer\MailerRecipientInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Component\Intl\Intl;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @ORM\Table(name="_user")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 * @ORM\HasLifecycleCallbacks
 * @UniqueEntity(
 *     fields="email",
 *     message="duplicate_email"
 * )
 */
class User implements AdvancedUserInterface, \Serializable, MailerRecipientInterface
{
    use ORMBehaviors\Timestampable\Timestampable;

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=128)
     * @Assert\NotBlank()
     */
    protected $firstName;

    /**
     * @var string
     * @ORM\Column(type="string", length=128)
     * @Assert\NotBlank()
     */
    protected $lastName;

    /**
     * @var string
     * @ORM\Column(type="string", length=128, nullable=true)
     */
    protected $zip;

    /**
     * @var string
     * @ORM\Column(type="string", length=2, nullable=true)
     */
    protected $country;

    /**
     * @var UserType
     * @ORM\ManyToOne(targetEntity="UserType")
     * @ORM\JoinColumn(onDelete="SET NULL", nullable=true)
     */
    protected $type;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $organization;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $website;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Application\Application", mappedBy="user", cascade={"remove"})
     */
    protected $applications;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Flux\Flux", mappedBy="user", cascade={"remove"})
     */
    protected $flux;

    /**
     * @var string
     * @ORM\Column(type="string", length=60, unique=true)
     * @Assert\NotBlank()
     * @Assert\Email(
     *     message = "L'email renseigné n'est pas valide."
     * )
     */
    private $email;

    /**
     * Plain password. Used for model validation. Must not be persisted.
     *
     * @var string
     */
    private $plainPassword;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    protected $dataGouvApiKey;

    /**
     * Encrypted password. Must be persisted.
     *
     * @var string
     * @ORM\Column(type="string", length=255)
     */
    private $password;

    /**
     * @var string
     * @ORM\Column(type="string", length=2)
     */
    private $locale;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $lastLogin;

    /**
     * @var bool
     * @ORM\Column(name="cgu", type="boolean", nullable=true)
     */
    protected $cgu;

    /**
     * @var bool
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $enabled;

    /**
     * @var bool
     * @ORM\Column(name="account_non_expired", type="boolean")
     */
    private $accountNonExpired;

    /**
     * @var bool
     * @ORM\Column(name="credentials_non_expired", type="boolean")
     */
    private $credentialsNonExpired;

    /**
     * @var bool
     * @ORM\Column(name="account_non_locked", type="boolean")
     */
    private $accountNonLocked;

    /**
     * @var array
     * @ORM\Column(type="string", length=24, nullable=false)
     */
    private $role;

    /**
     * User constructor.
     */
    public function __construct()
    {
        $this->applications = new ArrayCollection();
        $this->cgu = false;
        $this->enabled = false;
        $this->accountNonExpired = true;
        $this->credentialsNonExpired = true;
        $this->accountNonLocked = true;
        $this->role = 'ROLE_USER';
        //$this->country = 'FR';
        //$this->locale = 'fr';
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getFullName();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return string
     */
    public function getFullName()
    {
        return $this->firstName.' '.mb_strtoupper($this->lastName, 'UTF-8');
    }

    /**
     * @return string
     */
    public function getReverseFullName()
    {
        return mb_strtoupper($this->lastName, 'UTF-8').' '.$this->firstName;
    }

    /**
     * @return ArrayCollection
     */
    public function getApplications()
    {
        return $this->applications;
    }

    /**
     * @param ArrayCollection $applications
     */
    public function setApplications($applications)
    {
        $this->applications = $applications;
    }

    /**
     * @return ArrayCollection
     */
    public function getFlux()
    {
        return $this->flux;
    }

    /**
     * @param ArrayCollection $flux
     */
    public function setFlux($flux)
    {
        $this->flux = $flux;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = mb_strtolower($email, 'UTF-8');
    }

    /**
     * @return string
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * @param string $zip
     */
    public function setZip($zip)
    {
        $this->zip = $zip;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    public function getCountryName()
    {
        return Intl::getRegionBundle()->getCountryName($this->getCountry());
    }

    /**
     * @param string $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return string
     */
    public function getOrganization()
    {
        return $this->organization;
    }

    /**
     * @param string $organization
     */
    public function setOrganization($organization)
    {
        $this->organization = $organization;
    }

    /**
     * @return UserType
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param UserType $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * @param string $website
     */
    public function setWebsite($website)
    {
        $this->website = $website;
    }

    /**
     * @return string
     */
    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    /**
     * @param string $plainPassword
     */
    public function setPlainPassword($plainPassword)
    {
        $this->plainPassword = $plainPassword;
    }

    /**
     * @return string
     */
    public function getDataGouvApiKey()
    {
        return $this->dataGouvApiKey;
    }

    /**
     * @param string $dataGouvApiKey
     */
    public function setDataGouvApiKey($dataGouvApiKey)
    {
        $this->dataGouvApiKey = $dataGouvApiKey;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * @param string $locale
     */
    public function setLocale(string $locale)
    {
        $this->locale = $locale;
    }

    public function addPreviousPassword($previousPassword)
    {
    }

    public function getPreviousPasswords()
    {
        return array();
    }

    /**
     * @return \DateTime
     */
    public function getLastLogin()
    {
        return $this->lastLogin;
    }

    /**
     * @param \DateTime $lastLogin
     */
    public function setLastLogin($lastLogin)
    {
        $this->lastLogin = $lastLogin;
    }

    /**
     * @return bool
     */
    public function isCgu()
    {
        return $this->cgu;
    }

    /**
     * @param bool $cgu
     */
    public function setCgu($cgu)
    {
        $this->cgu = $cgu;
    }

    /**
     * @return bool
     */
    public function isEnabled()
    {
        return $this->enabled;
    }

    /**
     * @param bool $enabled
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
    }

    /**
     * @return bool
     */
    public function isAccountNonExpired()
    {
        return $this->accountNonExpired;
    }

    /**
     * @param bool $accountNonExpired
     */
    public function setAccountNonExpired($accountNonExpired)
    {
        $this->accountNonExpired = $accountNonExpired;
    }

    /**
     * @return bool
     */
    public function isCredentialsNonExpired()
    {
        return $this->credentialsNonExpired;
    }

    /**
     * @param bool $credentialsNonExpired
     */
    public function setCredentialsNonExpired($credentialsNonExpired)
    {
        $this->credentialsNonExpired = $credentialsNonExpired;
    }

    /**
     * @return bool
     */
    public function isAccountNonLocked()
    {
        return $this->accountNonLocked;
    }

    /**
     * @param bool $accountNonLocked
     */
    public function setAccountNonLocked($accountNonLocked)
    {
        $this->accountNonLocked = $accountNonLocked;
    }

    public function getSalt()
    {
        return null;
    }

    /**
     * @return array
     */
    public function getRoles()
    {
        return array($this->role);
    }

    /**
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @param string $role
     */
    public function setRole($role)
    {
        $this->role = $role;
    }

    /**
     * @param string|array $role
     *
     * @return bool
     */
    public function isRole($role)
    {
        if (is_string($role)) {
            return $role === $this->role;
        } elseif (is_array($role)) {
            foreach ($role as $_role) {
                if ($_role === $this->role) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @return bool
     */
    public function isMailingGranted($type, $data)
    {
        return true;
    }
    
    /**
     * {@inheritdoc}
     */
    public function eraseCredentials()
    {
        $this->plainPassword = null;
    }

    /**
     * @return bool
     */
    public function isLoginable()
    {
        if (!$this->accountNonLocked) {
            return false;
        }
        if (!$this->credentialsNonExpired) {
            return false;
        }
        if (!$this->accountNonExpired) {
            return false;
        }
        if (!$this->enabled) {
            return false;
        }

        return true;
    }

    /**
     * @return array
     */
    public function getAccountStatus()
    {
        if (!$this->accountNonLocked) {
            return array(
                'class' => 'danger',
                'label' => 'label.status.blocked',
            );
        }
        if (!$this->credentialsNonExpired) {
            return array(
                'class' => 'warning',
                'label' => 'label.status.expired',
            );
        }
        if (!$this->accountNonExpired) {
            return array(
                'class' => 'warning',
                'label' => 'label.status.expired',
            );
        }
        if (!$this->enabled) {
            return array(
                'class' => 'default',
                'label' => 'label.status.wait',
            );
        }
        if ($this->isRole('ROLE_USER_CERTIFIED')) {
            return array(
                'class' => 'success',
                'label' => 'label.status.certified',
            );
        }

        return array(
            'class' => 'success',
            'label' => 'label.status.validated',
        );
    }

    /**
     * @param LifecycleEventArgs $args
     * @ORM\PrePersist()
     */
    public function prePersistUpload(LifecycleEventArgs $args)
    {
        $this->upperName();
    }

    /**
     * @param PreUpdateEventArgs $args
     * @ORM\PreUpdate()
     */
    public function preUpdateUpload(PreUpdateEventArgs $args)
    {
        if ($args->hasChangedField('lastName') || $args->hasChangedField('firstName')) {
            $this->upperName();
        }
    }

    /** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->email,
            $this->enabled,
            $this->accountNonExpired,
            $this->credentialsNonExpired,
            $this->accountNonLocked,
            $this->role,
            // see section on salt below
            // $this->salt,
        ));
    }

    /** @see \Serializable::unserialize() */
    public function unserialize($serialized)
    {
        list(
            $this->id,
            $this->email,
            $this->enabled,
            $this->accountNonExpired,
            $this->credentialsNonExpired,
            $this->accountNonLocked,
            $this->role,
            // see section on salt below
            // $this->salt
            ) = unserialize($serialized);
    }

    protected function upperName()
    {
        $this->setLastName(mb_strtoupper($this->getLastName(), 'UTF-8'));
        $fc = mb_strtoupper(mb_substr($this->getFirstName(), 0, 1), 'UTF-8');
        $this->setFirstName($fc.mb_substr($this->getFirstName(), 1));
    }

    /**
     * @Assert\Callback
     */
    public function constraintCallback(ExecutionContextInterface $context)
    {
        if ($this->getType() && $this->getType()->isOrganization()) {
            if (!$this->getOrganization()) {
                $context->buildViolation('empty_organization')->atPath('organization')->addViolation();
            }
            if (!$this->getWebsite()) {
                $context->buildViolation('empty_website')->atPath('website')->addViolation();
            }
        }
    }
}
