<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Entity\User;

use AppBundle\Entity\AbstractType;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="user_type")
 * @ORM\Entity()
 */
class UserType extends AbstractType
{
    /**
     * @var bool
     *
     * @ORM\Column(name="is_organization", type="boolean", options={"default" : false})
     */
    private $isOrganization = false;

    /**
     * @var int
     *
     * @ORM\Column(name="weight", type="integer", options={"default" : 0})
     */
    private $weight = 0;

    /**
     * @return bool
     */
    public function isOrganization(): bool
    {
        return $this->isOrganization;
    }

    /**
     * @param bool $isOrganization
     */
    public function setIsOrganization(bool $isOrganization)
    {
        $this->isOrganization = $isOrganization;
    }

    /**
     * @return int
     */
    public function getWeight(): int
    {
        return $this->weight;
    }

    /**
     * @param int $weight
     */
    public function setWeight(int $weight)
    {
        $this->weight = $weight;
    }
}
