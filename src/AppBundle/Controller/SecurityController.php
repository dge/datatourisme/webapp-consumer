<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Controller;

use AppBundle\Entity\User\User;
use AppBundle\Form\Type\User\AccountType;
use AppBundle\Notification\Type\UserRegisterType;
use AppBundle\Utils\SSO\SSOPayload;
use AppBundle\Utils\SSO\SSOServer;
use Datatourisme\Bundle\WebAppBundle\Form\StrongPasswordType;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Validator\Constraints\Email;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

/**
 * Class SecurityController.
 */
class SecurityController extends Controller
{
    /**
     * @Route("/login", name="security.login")
     * @Security("!is_granted('IS_AUTHENTICATED_FULLY')")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function loginAction()
    {
        $authenticationUtils = $this->get('security.authentication_utils');
        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', array(
            'last_username' => $lastUsername,
            'error' => $error,
        ));
    }

    /**
     * @Route("/register", name="security.register")
     * @Security("!is_granted('IS_AUTHENTICATED_FULLY')")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function registerAction(Request $request)
    {
        $user = new User();
        $user->setPlainPassword(strval(rand(10000000, 9999999999999)));

        $form = $this->createForm(AccountType::class, $user);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            // check email exists
            $exists = $em->getRepository(User::class)->findOneBy(['email' => $user->getEmail()]);
            if (null == $exists) {
                // persist
                $em->persist($user);
                $em->flush();
                $emailTpl = 'account.register';
                // notify administrators
                $this->get('notifier')->dispatch(UserRegisterType::class, $user);
            } else {
                $user = $exists;
                $emailTpl = 'account.reset_password';
            }

            // send welcome/reset mail
            $now = new \DateTime();
            $bcrypt = $this->container->get('security.password_encoder')->encodePassword($user, $now->getTimestamp().$user->getPassword());
            // send link
            $this->get('webapp.mailer')->send($emailTpl, $user, array(
                'bcrypt' => $bcrypt,
                'expiration' => $this->getParameter('reset_password_expiration'),
                'timestamp' => $now->getTimestamp(),
            ));

            $this->addFlash('success', $this->get('translator')->trans('msg.register_mail_sent'));

            return $this->redirectToRoute('security.login');
        }

        return $this->render('security/register.html.twig', [
            'form' => $form->createView(),
            'user' => $user,
        ]);
    }

    /**
     * @Route("/cgu/accept", name="security.cgu.accept")
     * @Security("has_role('ROLE_USER')")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function cguAcceptAction(Request $request)
    {
        $cguPath = $this->getLocalizedPath($this->getParameter('cgu_md_path'), $request->getLocale());
        $content = file_get_contents($cguPath);
        $form = $this->createFormBuilder()
            ->add('accept', CheckboxType::class, array(
                'label' => 'option.cgu_accept',
                'required' => true,
                'data' => $this->getUser()->isCgu(),
            ))
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $accept = $form->get('accept')->getData();
            if ($accept) {
                $this->getUser()->setCgu(true);
                $this->getDoctrine()->getManager()->flush();

                return $this->redirect('/');
            }
            $this->addFlash('error', $this->get('translator')->trans('msg.cgu_required'));
        }

        return $this->render('security/cgu.html.twig', array(
            'content' => $content,
            'version' => $this->getParameter('cgu_version'),
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("cgu/download", name="security.cgu.download")
     * @Security("has_role('ROLE_USER')")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function cguDownloadAction(Request $request)
    {
        $cguPath = $this->getLocalizedPath($this->getParameter('cgu_pdf_path'), $request->getLocale());

        $cguVersion = $this->getParameter('cgu_version');
        $response = new BinaryFileResponse($cguPath);
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, 'datatourisme-cgu-diffuseur-v'.$cguVersion.'.pdf');

        return $response;
    }

    /**
     * @Route("/cgu.pdf", name="security.cgu.pdf")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function cguPdfAction(Request $request)
    {
        $cguPath = $this->getLocalizedPath($this->getParameter('cgu_pdf_path'), $request->getLocale());
        return new BinaryFileResponse($cguPath);
    }

    /**
     * @Route("/reset-password", name="security.reset_password")
     * @Security("!is_granted('IS_AUTHENTICATED_FULLY')")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function resetPasswordAction(Request $request)
    {
        $form = $this->createFormBuilder()
            ->add('email', EmailType::class, array(
                'label' => 'Email',
                'attr' => array(
                    'placeholder' => 'label.email',
                ),
                'constraints' => array(new Email()),
            ))
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            // get email
            $email = $form->get('email')->getData();
            /** @var QueryBuilder $qb */
            $qb = $this->getDoctrine()->getRepository(User::class)->createQueryBuilder('u');
            $qb->where('lower(u.email) = :email')
                ->setParameter('email', mb_strtolower($email, 'UTF-8'));
            $result = $qb->setMaxResults(1)->getQuery()->execute();
            $user = null;
            if (count($result) > 0) {
                $user = current($result);
            }

            if ($user && $user->isAccountNonLocked()) {
                // save lower email if it is not already
                if ($user->getEmail() !== mb_strtolower($email, 'UTF-8')) {
                    $user->setEmail(mb_strtolower($email, 'UTF-8'));
                    $this->getDoctrine()->getManager()->flush();
                }

                // generate new token
                $now = new \DateTime();
                $bcrypt = $this->container->get('security.password_encoder')->encodePassword($user, $now->getTimestamp().$user->getPassword());

                // send link
                $this->get('webapp.mailer')->send('account.reset_password', $user, array(
                    'bcrypt' => $bcrypt,
                    'expiration' => $this->getParameter('reset_password_expiration'),
                    'timestamp' => $now->getTimestamp(),
                ));
            }
            $this->addFlash('success', $this->get('translator')->trans('msg.reset_password_mail_sent'));

            return $this->redirectToRoute('security.login');
        }

        return $this->render('security/reset_password.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * Check reset link and manage user update password.
     *
     * @param Request $request
     * @param User    $user
     * @param string  $timestamp
     * @param string  $bcrypt
     *
     * @Route("/reset-password/{email}/{timestamp}/{bcrypt}", requirements={"bcrypt" = ".+"}, name="security.reset_password.process")
     * @ParamConverter("user", class="AppBundle:User\User", options={"email" = "email"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function resetPasswordProcessAction(Request $request, User $user, $timestamp, $bcrypt)
    {
        // compute number of seconds since link has been sent
        $elapsed = time() - $timestamp;

        // token is not expired
        if ($elapsed < $this->getParameter('reset_password_expiration')) {
            // check bcrypt validity with user salt and password
            $encoder = $this->container->get('security.encoder_factory')->getEncoder($user);
            $bcryptIsValid = $encoder->isPasswordValid($bcrypt, $timestamp.$user->getPassword(), $user->getSalt());
            if ($bcryptIsValid) {
                $form = $this->createFormBuilder($user)
                    ->add('plainPassword', StrongPasswordType::class, array('user' => $user))
                    ->getForm();

                $form->handleRequest($request);
                if ($form->isSubmitted() && $form->isValid()) {
                    $user->updateTimestamps();
                    $user->setCredentialsNonExpired(true);

                    // if user is new, send welcome mail & enable account
                    if (!$user->isEnabled()) {     
                        $user->setEnabled(true);
                        $this->get('webapp.mailer')->send('account.welcome', $user);
                    }

                    $this->getDoctrine()->getManager()->flush();
                    $this->addFlash('success', $this->get('translator')->trans('msg.reset_password_mail_sent'));

                    return $this->redirectToRoute('security.login');
                }

                return $this->render('security/reset_password_process.html.twig', [
                    'form' => $form->createView(),
                    'user' => $user,
                ]);
            }
        }

        $this->addFlash('warning', $this->get('translator')->trans('msg.reset_password_obsolete'));

        return $this->redirectToRoute('security.reset_password');
    }

    /**
     * @Route("/sso", name="_security.sso")
     *
     * @param Request $request
     *
     * @return RedirectResponse
     */
    public function ssoAction(Request $request)
    {
        $user = $this->getUser();
        if (!$user) {
            return new RedirectResponse($this->generateUrl('security.login').'?_target_path='.urlencode($request->getRequestUri()));
        }
        $sso = $request->get('sso');
        $signature = $request->get('sig');
        $secret = $this->getParameter('sso_secret');

        try {
            $payload = new SSOPayload($sso);
            $payload->validate($secret, $signature);
            $server = new SSOServer($payload, $secret);
            $server->setUserData($user);
            $data = $server->build();
        } catch (\Exception $e) {
            $this->get('logger')->err($e->getMessage());
            throw new BadRequestHttpException(null, $e);
        }

        return new RedirectResponse($this->getParameter('sso_response').'?'.$data);
    }

    /**
     * Return the localized path of a file, if exists
     */
    private function getLocalizedPath($path, $locale) {
        $pathParts = pathinfo($path);
        $localizedCguPath = $pathParts['dirname'].DIRECTORY_SEPARATOR.$pathParts['filename'].'.'.$locale.'.'.$pathParts['extension'];
        if (file_exists($localizedCguPath)) {
            $path = $localizedCguPath;
        }
        return $path;
    }
}
