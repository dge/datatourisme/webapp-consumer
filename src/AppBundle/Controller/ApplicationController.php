<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Application\Application;
use AppBundle\Form\Type\ApplicationType;
use Doctrine\ORM\Query\Expr\Join;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @Route("/application")
 */
class ApplicationController extends Controller
{
    /**
     * @Route("", name="application.index")
     * @Security("is_granted('application.see')")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $pageSize = 20;
        $em = $this->get('doctrine.orm.entity_manager');
        $query = $em->getRepository(Application::class)->createQueryBuilder('a')
            ->where('a.user = :user')
            ->andWhere('a.deleted = false')
            ->addGroupBy('a.id')
            ->setParameter('user', $this->getUser());

        // organisation types
        $query->leftJoin('AppBundle:Application\ApplicationType', 't', Join::WITH, 't = a.type')
            ->addGroupBy('t.id');

        $paginator = $this->get('knp_paginator');
        $list = $paginator->paginate($query, $request->query->getInt('page', 1), $pageSize, ['distinct' => false]);

        return $this->render('application/index.html.twig', array(
            'page_size' => $pageSize,
            'list' => $list,
        ));
    }

    /**
     * @Route("/add", name="application.add")
     * @Security("is_granted('application.create')")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function addAction(Request $request)
    {
        $application = new Application($this->getUser());

        return $this->editAction($request, $application);
    }

    /**
     * @Route("/{id}/edit", name="application.edit")
     * @Security("is_granted('application.edit', application)")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function editAction(Request $request, Application $application)
    {
        $form = $this->createForm(ApplicationType::class, $application);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            if (!$application->getApiKey()) {
                // in case uuid is not unique
                $continue = true;
                while ($continue) {
                    $application->setApiKey(Uuid::uuid4());
                    $existingApp = $this->getDoctrine()->getRepository('AppBundle:Application\Application')->findBy(array('apiKey' => $application->getApiKey()));
                    $continue = count($existingApp) > 0;
                }
            }
            $em->persist($application);
            $em->flush();
            $this->addFlash('success', $this->get('translator')->trans('msg.application_updated', ['%name%' => $application->getName()]));

            return $this->redirectToRoute('application.index');
        }

        return $this->render('application/edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/remove", name="application.remove")
     * @Security("is_granted('application.remove', application)")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function removeAction(Request $request, Application $application)
    {
        if ('POST' === $request->getMethod()) {
            $application->setDeleted(true);
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', $this->get('translator')->trans('msg.application_deleted'));

            return new JsonResponse([
                'success' => true,
                'redirect' => $this->generateUrl('application.index'),
            ], 200);
        }

        return $this->render('application/remove.html.twig', [
            'application' => $application,
        ]);
        
    }
}
