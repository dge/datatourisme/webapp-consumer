<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Controller\Administration;

use AppBundle\Entity\Flux\Download;
use Datatourisme\Bundle\WebAppBundle\Form\DateRangePickerType;
use Datatourisme\Bundle\WebAppBundle\Utils\CsvWriter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Route("/admin/download")
 * @Security("has_role('ROLE_ADMIN')")
 */
class DownloadController extends Controller
{
    /**
     * @Route("/export", name="admin.download.export")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function exportAction(Request $request)
    {
        $form = $this->createFormBuilder()
            ->add('datePicker', DateRangePickerType::class, array(
                'label' => 'label.select_periode',
                'required' => false,
            ))
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $repository = $this->getDoctrine()->getManager()->getRepository(Download::class);
            $entities = $repository->findBetweenDateRange($form->get('datePicker')->get('start')->getData(), $form->get('datePicker')->get('end')->getData());

            $writer = new CsvWriter([
                'Id' => 'id',
                'Utilisateur' => 'flux.user.reverseFullName',
                'Flux' => 'flux.name',
                'Format' => 'outputType.name',
                'Date' => 'createdAt',
                'Méthode' => function ($entity) {
                    return 'manual' === $entity->getType() ? 'Manuelle' : 'Webservice';
                },
                'Application' => 'application.name',
                'Type d\'application' => 'application.type',
                'Nombre de POI' => 'nbrPOI',
            ]);

            $writer->writeObjects($entities);
            $writer->output('downloads.csv');
            exit;
        }

        return $this->render('admin/flux/report_download.html.twig', array(
            'form' => $form->createView(),
        ));
    }
}
