<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Controller\Administration;

use AppBundle\Entity\User\User;
use AppBundle\Form\Type\User\AccountType;
use AppBundle\Repository\UserRepository;
use Datatourisme\Bundle\WebAppBundle\Utils\CsvWriter;
use AppBundle\Form\Filter\UserFilter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Route("/admin/user")
 * @Security("is_granted('user.administrate')")
 */
class UserController extends Controller
{
    /**
     * @Route("", name="user.index")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $pageSize = 20;
        $paginationOptions = array(
            'defaultSortFieldName' => 'u.createdAt',
            'defaultSortDirection' => 'DESC',
        );

        $em = $this->get('doctrine.orm.entity_manager');

        /** @var UserRepository $userRepository */
        $userRepository = $em->getRepository(User::class);
        $qb = $userRepository->getIndexQueryBuilder();

        $list = $this->get('webapp.filterable_paginated_list_factory')
            ->getList($qb, UserFilter::class)
            ->setPageSize($pageSize)
            ->setPaginationOptions($paginationOptions)
            ->handleRequest($request);

        return $this->render('admin/user/index.html.twig', array(
            'list' => $list,
            'page_size' => $pageSize,
        ));
    }

    /**
     * @Route("/add", name="user.add")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function addAction(Request $request)
    {
        // init a new User
        $account = new User();
        $account->setRole('ROLE_ADMIN');
        $account->setPlainPassword(strval(rand(10000000, 9999999999999)));
        $form = $this->createForm(AccountType::class, $account);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($account);
            $em->flush();

            // generate new token
            $now = new \DateTime();
            $bcrypt = $this->get('security.password_encoder')->encodePassword($account, $now->getTimestamp().$account->getPassword());

            // send link
            $this->get('webapp.mailer')->send('account.register', $account, array(
                'bcrypt' => $bcrypt,
                'expiration' => $this->getParameter('reset_password_expiration'),
                'timestamp' => $now->getTimestamp(),
            ));

            $this->addFlash('success', $this->get('translator')->trans('msg.admin_created'));

            return $this->redirectToRoute('user.edit', array('id' => $account->getId()));
        }

        return $this->render('admin/user/add.html.twig', [
            'form' => $form->createView(),
            'user' => $account,
        ]);
    }

    /**
     * @Route("/edit/{id}", name="user.edit")
     * @Security("is_granted('user.edit', account)")
     *
     * @param Request $request
     * @param User    $account
     *
     * @return Response
     */
    public function editAction(Request $request, User $account)
    {
        $accountForm = $this->createForm(AccountType::class, $account);
        $accountForm->handleRequest($request);
        if ($accountForm->isSubmitted() && $accountForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', $this->get('translator')->trans('msg.user_updated'));

            return $this->redirectToRoute('user.edit', array('id' => $account->getId()));
        }

        return $this->render('admin/user/edit.html.twig', [
            'form' => $accountForm->createView(),
            'user' => $account,
        ]);
    }

    /**
     * @Route("/edit/{id}/administrate", name="user.administrate")
     * @Security("is_granted('user.administrate', account)")
     *
     * @param User|null $account
     *
     * @return Response
     */
    public function administrateAction(User $account)
    {
        return $this->render('admin/user/administrate.html.twig', [
            'user' => $account,
        ]);
    }

    /**
     * @Route("/edit/{id}/administrate/certify", name="user.administrate.certify")
     * @Security("is_granted('user.administrate', account)")
     *
     * @param Request $request
     * @param User    $account
     *
     * @return Response
     */
    public function certifyAction(Request $request, User $account)
    {
        if ('POST' === $request->getMethod()) {
            if ($account->isRole('ROLE_USER_CERTIFIED')) {
                $account->setRole('ROLE_USER');
            } else {
                $account->setRole('ROLE_USER_CERTIFIED');
            }
            $this->getDoctrine()->getManager()->flush();
            $this->get('webapp.mailer')->send('account.certify', $account, array('certified' => $account->isRole('ROLE_USER_CERTIFIED')));
            $this->addFlash('success', $this->get('translator')->trans($account->isRole('ROLE_USER_CERTIFIED') ? 'msg.user_certified' : 'msg.user_uncertified'));

            return new JsonResponse([
                'success' => true,
                'reload' => true,
            ], 200);
        }

        return $this->render('admin/user/administrate/certify.html.twig', [
            'user' => $account,
        ]);
    }

    /**
     * @Route("/edit/{id}/administrate/block", name="user.administrate.block")
     * @Security("is_granted('user.administrate', account)")
     *
     * @param Request $request
     * @param User    $account
     *
     * @return Response
     */
    public function blockAction(Request $request, User $account)
    {
        if ('POST' === $request->getMethod()) {
            $account->setAccountNonLocked(!$account->isAccountNonLocked());
            $this->getDoctrine()->getManager()->flush();
            $this->get('webapp.mailer')->send('account.block', $account);
            $this->addFlash(!$account->isAccountNonLocked() ? 'warning' : 'success', $this->get('translator')->trans(!$account->isAccountNonLocked() ? 'msg.user_blocked' : 'msg.user_unblocked'));

            return new JsonResponse([
                'success' => true,
                'reload' => true,
            ], 200);
        }

        return $this->render('admin/user/administrate/block.html.twig', [
            'user' => $account,
        ]);
    }

    /**
     * @Route("/edit/{id}/administrate/remove", name="user.administrate.remove")
     * @Security("is_granted('user.administrate', account)")
     *
     * @param Request $request
     * @param User    $account
     *
     * @return RedirectResponse|Response
     */
    public function removeAction(Request $request, User $account)
    {
        if ('POST' === $request->getMethod()) {
            $em = $this->getDoctrine()->getManager();
            $this->get('webapp.mailer')->send('account.delete', $account);
            $em->remove($account);
            $em->flush();
            $this->addFlash('success', $this->get('translator')->trans('msg.user_deleted'));

            return new JsonResponse([
                'success' => true,
                'redirect' => $this->generateUrl('user.index'),
            ], 200);
        }

        return $this->render('admin/user/administrate/remove.html.twig', [
            'user' => $account,
        ]);
    }

    /**
     * @Route("/export", name="user.export")
     *
     * @param Request $request
     */
    public function exportAction(Request $request)
    {
        $writer = new CsvWriter([
            'Nom' => 'lastName',
            'Prénom' => 'firstName',
            'Email' => 'email',
            'Code postal' => 'zip',
            'Pays' => 'country',
            'Profil' => 'type.name',
            'Structure' => 'organization',
            'Site web' => 'website',
            'CGU' => 'cgu',
            'Inscription' => 'createdAt',
            'Status' => 'accountStatus[label]',
            'Nombre de flux' => 'flux.count',
        ]);

        /** @var UserRepository $userRepository */
        $userRepository = $this->getDoctrine()->getManager()->getRepository(User::class);
        $qb = $userRepository->getIndexQueryBuilder();

        // apply filter
        $form = $this->createForm(UserFilter::class);
        $form->submit($request->query->get($form->getName()));
        $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($form, $qb);

        $iter = $qb->getQuery()->iterate();
        foreach ($iter as $row) {
            /* @var User $user */
            $writer->writeObject($row[0]);
        }

        $writer->output('users.csv');
        exit;
    }
}
