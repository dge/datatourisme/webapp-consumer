<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Controller\Flux;

use AppBundle\Entity\Flux\Flux;
use AppBundle\Entity\Flux\OutputType;
use AppBundle\Form\Filter\FluxFilter;
use AppBundle\Form\Flow\FluxWizardFlow;
use AppBundle\JavaWorker\JavaWorkerClient;
use AppBundle\Repository\FluxRepository;
use AppBundle\Utils\FluxWizardHelper;
use Behat\Transliterator\Transliterator;
use GuzzleHttp\Exception\RequestException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * @Route("/flux")
 * @Security("has_role('ROLE_USER')")
 */
class FluxController extends Controller
{
    /**
     * @Route("", name="flux.index")
     * @Security("is_granted('flux.see')")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $pageSize = 20;
        $paginationOptions = array(
            'defaultSortFieldName' => 'f.lastProcessAt',
            'defaultSortDirection' => 'DESC',
        );

        /** @var FluxRepository $fluxRepository */
        $fluxRepository = $this->get('app.repository.flux');

        $qb = $fluxRepository->getIndexQueryBuilder();

        $list = $this->get('webapp.filterable_paginated_list_factory')
            ->getList($qb, FluxFilter::class, null, array('userIsAdmin' => $this->isGranted('ROLE_ADMIN', $this->getUser())))
            ->setPageSize($pageSize)
            ->setPaginationOptions($paginationOptions)
            ->setQueryHint($fluxRepository->getCount())
            ->handleRequest($request);

        return $this->render('flux/index.html.twig', array(
            'list' => $list,
            'page_size' => $pageSize,
        ));
    }

    /**
     * @Route("/create", name="flux.create")
     * @Security("is_granted('flux.create')")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function createAction(Request $request)
    {
        // create flow
        $flow = $this->get(FluxWizardFlow::class);
        $formData = $flow->generateFormObject();
        $flow->bind($formData);
        $form = $flow->createForm();

        if ($flow->isValid($form)) {
            $flow->saveCurrentStepData($form);
    
            if ($flow->nextStep()) {
                // form for the next step
                $form = $flow->createForm();
            } else {
                // flow finished
                $helper = $this->get(FluxWizardHelper::class);
                $queryData = $helper->createQueryData($formData->types, $formData->locations);
                $query = $helper->createConstructQuery($formData->types, $formData->locations);

                // create flux
                $flux = new Flux($this->getUser());
                $flux->setName($formData->name);
                $flux->setDescription($formData->description);
                $flux->setOutputType($formData->outputType);
                $flux->setQueryType(OutputType::TYPE_CONSTRUCT);
                $flux->setQueryData($queryData);
                $flux->setQuery($query);
                
                // persist
                $em = $this->getDoctrine()->getManager();
                $em->persist($flux);
                $em->flush();
                $this->addFlash('success', $this->get('translator')->trans('msg.flux_created'));
    
                $flow->reset(); // remove step data from the session
                return $this->redirect($this->generateUrl('flux.parameters', ['id' => $flux->getId()])); // redirect when done
            }
        }
        
        return $this->render('flux/create/step' . $flow->getCurrentStepNumber() . '.html.twig', [
            'form' => $form->createView(),
            'formData' => $formData,
            'flow' => $flow
        ]);
    }

    /**
     * @Route("/create/preview", name="flux.create.preview", options={ "i18n"= false })
     * @Security("is_granted('flux.create')")
     * @Method({"POST", "OPTIONS"})
     *
     * @param Request $request
     *
     * @return Response
     */
    public function createPreviewAction(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        $helper = $this->get(FluxWizardHelper::class);
        $query = $helper->createCountQuery($data['types']?:[], $data['locations']?:[]);
        $javaWorkerClient = $this->get('app.java_worker.client');
        try {
            $data = json_decode($request->getContent(), true);
            $data = $javaWorkerClient->sparqlPreview($query);
            $count = (int) $data['results']['bindings'][0]['count']['value'];
            
            return JsonResponse::create(['count' => $count]);
        } catch (RequestException $e) {
            return JavaWorkerClient::handleRequestException($e);
        }
    }

    /**
     * @Route("/help/output/{id}", name="flux.help.output")
     *
     * @param OutputType $type
     *
     * @return Response
     */
    public function helpOutputAction(OutputType $type)
    {
        $name = Transliterator::unaccent($type->getName());
        $template = implode(
            DIRECTORY_SEPARATOR,
            [$type->getRequestType(), $type->getCode()]
        );

        return $this->render("flux/help/output/$template.html.twig");
    }
}
