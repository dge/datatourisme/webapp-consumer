<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Controller\Flux;

use AppBundle\Entity\Flux\Flux;
use AppBundle\JavaWorker\JavaWorkerClient;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Form\Type\Flux\ExpertEditorType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

/**
 * @Route("/flux/{id}/editor")
 * @Security("is_granted('flux.edit', flux)")
 * @ParamConverter("flux", class="AppBundle:Flux\Flux")
 */
class FluxEditorController extends Controller
{
    /**
     * @Route("", name="flux.editor")
     *
     * @param Request $request
     * @param Flux    $flux
     *
     * @return Response
     */
    public function editorAction(Request $request, Flux $flux): Response
    {
        switch ($flux->getMode()) {
            case Flux::MODE_VISUAL:
                return $this->render('flux/detail/editor/visual.html.twig', array('flux' => $flux));
            case Flux::MODE_EXPERT:
                return $this->expertAction($flux, $request);
        }

        throw new \LogicException("This code should not be reached. Something really bad happened with Flux's mode.");
    }

    /**
     * @param Request $request
     * @param Flux    $flux
     *
     * @return Response
     */
    private function expertAction(Flux $flux, Request $request): Response
    {
        $data = [];
        $form = $this->createForm(ExpertEditorType::class, $flux);

        $form = $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid() && Flux::MODE_EXPERT === $flux->getMode()) {
            // Save the sparql request
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', $this->get('translator')->trans('msg.query_updated'));

            return $this->redirectToRoute('flux.editor', array('id' => $flux->getId()));
        }

        return $this->render('flux/detail/editor/expert.html.twig', array(
            'form' => $form->createView(),
            'data' => $data,
            'flux' => $flux,
        ));
    }

    /**
     * @Route("/preview", name="flux.editor.preview")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function editorPreviewAction(Flux $flux, Request $request): Response
    {
        $query = $request->get('query');
        if ('POST' === $request->getMethod()) {
            $javaWorkerClient = $this->get('app.java_worker.client');
            try {
                $data = json_decode($request->getContent(), true);
                $data = $javaWorkerClient->sparqlPreview($data['query']);

                return JsonResponse::create($data);
            } catch (RequestException $e) {
                return JavaWorkerClient::handleRequestException($e);
            }
        }

        return $this->render('flux/detail/editor/preview.html.twig', array(
            'query' => $query,
            'flux' => $flux,
        ));
    }

    /**
     * @Route("/visual", name="flux.editor.visual")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function editorVisualAction(Flux $flux, Request $request): Response
    {
        if ('POST' == $request->getMethod()) {
            try {
                $data = json_decode($request->getContent(), true);
                if (!$data['query'] || !$data['sparql']) {
                    return JsonResponse::create(['error' => 'Impossible de sauvegarder la requête.'], 400);
                }
                $flux->setQueryData($data['query']);
                $flux->setQuery($data['sparql']);
                $this->get('doctrine.orm.entity_manager')->flush();
                $this->addFlash('success', $this->get('translator')->trans('msg.query_updated'));

                return JsonResponse::create([
                    'success' => true,
                    'redirect' => $this->generateUrl('flux.editor', array('id' => $flux->getId())),
                ]);
            } catch (\Exception $e) {
                $this->get('logger')->err($e->getMessage());
                return JsonResponse::create(['error' => $e->getMessage()], 500);
            }
        }

        return $this->render('flux/detail/editor/visual_editor.html.twig', array(
            'flux' => $flux,
        ));
    }

    /**
     * @Route("/switch_expert", name="flux.editor.switch_to_expert")
     *
     * @param Flux    $flux
     * @param Request $request
     *
     * @return Response
     */
    public function switchToExpertAction(Flux $flux, Request $request): Response
    {
        if ('POST' === $request->getMethod()) {
            $flux->setMode(Flux::MODE_EXPERT);
            $this->get('doctrine.orm.entity_manager')->flush();

            return $this->redirectToRoute('flux.editor', array(
                'id' => $flux->getId(),
            ));
        }

        return $this->render('flux/detail/editor/switch_to_expert.html.twig', array(
            'flux' => $flux,
        ));
    }
}
