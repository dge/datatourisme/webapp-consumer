<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Controller;

use AppBundle\Entity\User\User;
use AppBundle\Form\Type\User\DataGouvApiKeyType;
use AppBundle\Form\Type\User\AccountType;
use Datatourisme\Bundle\WebAppBundle\Form\UpdatePasswordType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Route("/account")
 * @Security("is_granted('user.edit', user)")
 */
class AccountController extends Controller
{
    /**
     * @Route("", name="account.user")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function userAction(Request $request)
    {
        $userForm = $this->createForm(AccountType::class, $this->getUser());

        $userForm->handleRequest($request);
        if ($userForm->isSubmitted() && $userForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', $this->get('translator')->trans('msg.account_updated'));

            return $this->redirectToRoute('account.user');
        }

        return $this->render('account/user.html.twig', [
            'form' => $userForm->createView(),
            'user' => $this->getUser(),
        ]);
    }

    /**
     * @Route("/data-gouv", name="account.datagouv")
     * @Security("is_granted('user.datagouv', user)")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function dataGouvAction(Request $request)
    {
        $form = $this->createForm(DataGouvApiKeyType::class, $this->getUser());
        $form = $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', $this->get('translator')->trans('msg.key_saved'));

            return $this->redirectToRoute('account.datagouv');
        }

        return $this->render('account/datagov.html.twig', [
            'form' => $form->createView(),
            'user' => $this->getUser(),
        ]);
    }

    /**
     * @Route("/administrate", name="account.administrate")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function administrateAction()
    {
        return $this->render('account/administrate.html.twig', [
            'user' => $this->getUser(),
        ]);
    }

    /**
     * @Route("/administrate/password", name="account.administrate.password")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function administratePasswordAction(Request $request)
    {
        /** @var User $account */
        $account = $this->getUser();
        $form = $this->createForm(UpdatePasswordType::class, $account);
        $em = $this->getDoctrine()->getManager();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $account->updateTimestamps();
            $em->flush();
            $this->addFlash('success', $this->get('translator')->trans('msg.password_updated'));

            return new JsonResponse([
                'success' => true,
                'reload' => true,
            ], 200);
        }

        return $this->render('account/administrate/password.html.twig', [
            'form' => $form->createView(),
            'user' => $this->getUser(),
        ]);
    }
}
