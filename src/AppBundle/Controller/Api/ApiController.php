<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Controller\Api;

use AppBundle\JavaWorker\JavaWorkerClient;
use Datatourisme\Api\DatatourismeApi;
use GuzzleHttp\Exception\RequestException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Youshido\GraphQL\Parser\Parser;

/**
 * @Route("/api")
 */
class ApiController extends Controller
{
    /**
     * @Route("/ontology/{profile}", name="api.ontology", defaults={"profile" = null})
     * @Method({"GET"})
     *
     * @param null $profile
     *
     * @return JsonResponse
     */
    public function ontologyAction($profile = null): JsonResponse
    {
        $javaWorkerClient = $this->get('app.java_worker.client');
        try {
            $ontology = $javaWorkerClient->ontology($profile);

            return JsonResponse::create($ontology);
        } catch (RequestException $e) {
            return JavaWorkerClient::handleRequestException($e);
        }
    }

    /**
     * @Route("/graphql", name="api.graphql")
     * @Method({"POST", "OPTIONS"})
     *
     * @return JsonResponse
     */
    public function graphqlAction(Request $request): JsonResponse
    {
        /** @var DatatourismeApi $api */
        $api = $this->get('app.graphql.api');

        $requestData = json_decode($request->getContent() ?: '', true);
        $payload = isset($requestData['query']) ? $requestData['query'] : null;

        // check payload to avoid from and size arguments usage
        $parser = new Parser();
        $data = $parser->parse($payload);
        foreach ($data['queries'] as $query) {
            if ($query->getArgumentValue('size') || $query->getArgumentValue('from')) {
                throw new BadRequestHttpException('This endpoint can not handle from or size argument.');
            }
        }

        $response = $api->process($payload);

        return JsonResponse::create($response);
    }
}
