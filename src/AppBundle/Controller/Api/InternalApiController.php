<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Controller\Api;

use AppBundle\Entity\Flux\Flux;
use AppBundle\Entity\Flux\Process;
use AppBundle\Notification\Type\FluxDeprecatedType;
use AppBundle\Notification\Type\ProcessCompleteType;
use AppBundle\Process\Report\MessageManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * @Route("/internal/api", options={ "i18n"= false })
 */
class InternalApiController extends Controller
{
    /**
     * Pull file and start process.
     *
     * @Route("/run/{id}", name="internal.api.run")
     *
     * @param Flux $flux
     *
     * @return JsonResponse
     */
    public function runAction(Flux $flux)
    {
        if (!$flux->isRunnable()) {
            throw new BadRequestHttpException('Le flux ne peux pas être lancé.');
        }

        // check usage
        $em = $this->getDoctrine()->getManager();
        $now = new \DateTime();
        if ($flux->getUsedAt() && $now->diff($flux->getUsedAt())->days > 7 && !$flux->isDataGouv()) {
            // if the flux has not been used from 7 days, de-activate it
            $flux->setScheduleHour(null);
            $em->flush();
            $this->get('notifier')->dispatch(FluxDeprecatedType::class, $flux);

            return new JsonResponse(['success' => true], 200);
        }

        // go !
        $this->get('app.process_cache')->generateFluxCacheData($flux, Process::TYPE_COMPLETE);

        return new JsonResponse(['success' => true], 200);
    }

    /**
     * monitor messages from worker.
     *
     * @Route("/status/{type}", name="internal.api.status")
     * @Method({"POST"})
     *
     * @param Request $request
     * @param $type
     *
     * @return JsonResponse
     *
     * @throws \Exception
     */
    public function statusAction(Request $request, $type)
    {
        $em = $this->getDoctrine()->getManager();
        $payload = json_decode($request->getContent(), true);
        if (!isset($payload['uuid'])) {
            throw new \Exception('process doesn\'t exist');
        }

        $processId = (int) $payload['uuid'];
        /** @var Process $process */
        $process = $em->getRepository(Process::class)->find($processId);
        if (!$process) {
            throw new \Exception('process doesn\'t exist');
        }

        $em->getConnection()->beginTransaction();

        $messageManager = new MessageManager($payload);

        try {
            $process = $messageManager->process($em, $process);
            switch ($type) {
                case 'start':
                    $process->setStatus(Process::STATUS_RUNNING);
                    break;
                case 'success':
                    // notify user
                    if (Process::TYPE_PARTIAL == $process->getType() ||
                        (Process::TYPE_COMPLETE == $process->getType()) && !$process->getFlux()->getExecutedAt()) {
                        $this->get('notifier')->dispatch(ProcessCompleteType::class, $process);
                    }
                    $process->setStatus(Process::STATUS_OK);

                    // only for complete
                    if (Process::TYPE_COMPLETE == $process->getType()) {
                        // update set executed at
                        $process->getFlux()->setExecutedAt(new \DateTime());

                        // keep only 5 last downloads
                        /** @var QueryBuilder $query */
                        $query = $em->getRepository(Process::class)->cleanOldEntriesQueryBuilder($process->getFlux(), 5, Process::TYPE_COMPLETE);
                        $query->getQuery()->execute();
                    }

                    break;
                case 'error':
                    $process->setStatus(Process::STATUS_ERROR);
                    break;
            }
        } catch (\Exception $e) {
            $this->get('logger')->err($e->getMessage());
            $em->getConnection()->rollBack();

            return new JsonResponse(['error' => $e->getMessage()], 500);
        }

        $em->getConnection()->commit();
        $em->flush();

        return new JsonResponse(['success' => 'success']);
    }
}
