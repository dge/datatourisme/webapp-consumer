<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function homepageAction(Request $request)
    {
        $client = $this->get('discourse.client');

        // categories
        $response = $client->getSite();
        $categories = array_combine(
            array_map(function($cat) { return $cat->id; }, $response->categories),
            $response->categories
        );

        // latest topics
        $response = $client->getLatestTopics();
        $latestTopics = array_slice(array_filter($response->topic_list->topics, function($topic) {
            return $topic->category_id <> 7;
        }), 0, 10);
    
        // latest announcements
        $response = $client->getCategoryLatestTopics(7);
        $latestAnnouncements = array_slice(array_filter($response->topic_list->topics, function($topic) {
            return !$topic->pinned || in_array('sondage', $topic->tags);
        }), 0, 3);

        if (count($latestAnnouncements)) {
            $topic = &$latestAnnouncements[0];
            $response = $client->getTopic($topic->id);
            $topic->post = $response->post_stream->posts[0];
        }

        return $this->render('homepage.html.twig', [
            'categories' => $categories,
            'latestTopics' => $latestTopics,
            'latestAnnouncements' => $latestAnnouncements,
            'discourse' => $client
        ]);
    }
}
