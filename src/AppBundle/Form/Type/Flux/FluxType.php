<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Form\Type\Flux;

use AppBundle\Entity\Flux\Flux;
use AppBundle\Entity\Flux\OutputType;
use AppBundle\Utils\DataGouvApi;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Translation\Translator;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Translation\TranslatorInterface;

class FluxType extends AbstractType
{
    /**
     * AuthorizationChecker.
     */
    protected $authorizationChecker;

    /**
     * @var DataGouvApi
     */
    protected $dataGouvApi;

    /**
     * @var TranslatorInterface
     */
    protected $translator;

    /**
     * @var RouterInterface
     */
    protected $router;

    /**
     * FluxType constructor.
     *
     * @param AuthorizationChecker $authorizationChecker
     * @param DataGouvApi          $dataGouvApi
     * @param TranslatorInterface  $translator
     * @param RouterInterface      $router
     */
    public function __construct(AuthorizationChecker $authorizationChecker, DataGouvApi $dataGouvApi, TranslatorInterface $translator, RouterInterface $router)
    {
        $this->authorizationChecker = $authorizationChecker;
        $this->dataGouvApi = $dataGouvApi;
        $this->translator = $translator;
        $this->router = $router;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array(
                'label' => 'label.name',
            ))

            ->add('description', TextareaType::class, array(
                'label' => 'label.description',
                'attr' => array(
                    'rows' => 3,
                ),
            ));

        $builder->addEventListener(FormEvents::PRE_SET_DATA, array($this, 'onPreSetData'));
    }

    /**
     * @param FormEvent $event
     */
    public function onPreSetData(FormEvent $event)
    {
        /** @var Flux $flux */
        $flux = $event->getData();
        $form = $event->getForm();

        $this->addOutputTypeForm($form, $flux);
        $this->addLanguageForm($form, $flux);
        $this->addDatagouvForm($form, $flux);
    }

    /**
     * @param $form
     * @param Flux $flux
     */
    public function addOutputTypeForm(FormInterface $form, Flux $flux)
    {
        $attrs = [
            'label' => 'label.output_format',
            'class' => OutputType::class,
        ];
        $queryType = $flux->getQueryType();
        if (!$queryType) {
            $form->add('outputType', EntityType::class, $attrs + array(
                'query_builder' => function (EntityRepository $er) use ($queryType) {
                    return $er->createQueryBuilder('ot')
                        ->orderBy('ot.default', 'desc');
                },
                'help' => $this->translator->trans('help.output_format')
                    .' <strong>'.$this->translator->trans('help.output_format_need_query').'</strong>',
                'disabled' => true,
            ));
        } else {
            $form->add('outputType', EntityType::class, $attrs + array(
                'query_builder' => function (EntityRepository $er) use ($queryType) {
                    return $er->createQueryBuilder('ot')
                        ->where('ot.requestType = :requestType OR ot.requestType is null')
                        ->setParameter('requestType', $queryType);
                },
                'group_by' => 'type',
                'help' => $this->translator->trans('help.output_format')
                .' <a href="#" class="ouput-format-preview-link" data-toggle="modal-remote">'.$this->translator->trans('help.output_format_preview_link').'</a>',
            ));
        }
    }

    /**
     * @param $form
     * @param Flux $flux
     */
    public function addLanguageForm(FormInterface $form, Flux $flux)
    {
        $outputType = $flux->getOutputType();
        if ($outputType && $outputType->isLocalized()) {
            $form->add('languages', ChoiceType::class, array(
                'label' => 'label.locales',
                'expanded' => true,
                'multiple' => true,
                'choices' => array(
                    'label.locale.fr' => 'fr',
                    'label.locale.en' => 'en',
                    'label.locale.de' => 'de',
                    'label.locale.nl' => 'nl',
                    'label.locale.it' => 'it',
                    'label.locale.es' => 'es',
                    'label.locale.ru' => 'ru',
                    'label.locale.zh' => 'zh',
                ),
                'choice_attr' => function ($key, $val, $index) {
                    if (!in_array($key, ['fr', 'en'])) {
                        return ['disabled' => 'disabled', 'title' => 'Cette langue n\'est pas encore disponible'];
                    }
    
                    return [];
                },
            ));
        }
    }

    /**
     * @param $form
     * @param $flux
     */
    private function addDatagouvForm($form, $flux)
    {
        if ($this->authorizationChecker->isGranted('user.datagouv', $flux->getUser())) {
            $datasets = $this->dataGouvApi->datasets();
            $form->add('dataGouv', CheckboxType::class, array(
                'label' => 'label.publish_datagouv',
                'required' => false,
                'disabled' => !is_array($datasets),
                'help' => !is_array($datasets) ?
                    $this->translator->trans('help.publish_datagouv_missing_datasets', [
                        '%url_datagouv%' => 'https://www.data.gouv.fr/fr/admin/me',
                        '%url%' => $this->router->generate('account.datagouv'),
                    ]) : 'help.publish_datagouv',
            ));

            if (is_array($datasets)) {
                $choices = array();
                foreach ($datasets as $dataset) {
                    $choices[$dataset->title] = $dataset->id;
                }

                $form->add('dataSet', ChoiceType::class, array(
                    'label' => 'label.datagouv_dataset',
                    'choices' => $choices,
                    'required' => false,
                    'group_attr' => array(
                        'data-visibility-target' => '#flux_dataGouv',
                        'data-visibility-value' => true,
                        'class' => 'required',
                    ),
                ));
            }
        }
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Flux::class,
        ));
    }
}
