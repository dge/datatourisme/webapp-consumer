<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Form\Filter;

use AppBundle\Entity\User\UserType;
use Doctrine\ORM\EntityRepository;
use Lexik\Bundle\FormFilterBundle\Filter\Doctrine\ORMQuery;
use Lexik\Bundle\FormFilterBundle\Filter\FilterOperands;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type\ChoiceFilterType;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type\EntityFilterType;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type\TextFilterType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Intl\Intl;

/**
 * Class UserFilter.
 */
class UserFilter extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $defaultPlaceholder = 'placeholder.all';

        $builder->add('fullName', TextFilterType::class, array(
            'apply_filter' => function (ORMQuery $filterQuery, $field, $values) {
                if (empty($values['value'])) {
                    return null;
                }
                // expressions that represent the condition
                $builder = $filterQuery->getExpressionBuilder();
                $expression = $builder->expr()->orX(
                    $builder->stringLike('u.lastName', $values['value'], FilterOperands::STRING_CONTAINS),
                    $builder->stringLike('u.firstName', $values['value'], FilterOperands::STRING_CONTAINS)
                );

                return $filterQuery->createCondition($expression);
            },
        ));
        $builder->add('organization', TextFilterType::class);

        $builder->add('type', EntityFilterType::class, array(
            'class' => UserType::class,
            'placeholder' => $defaultPlaceholder,
            'choice_translation_domain' => 'entities',
            'query_builder' => function (EntityRepository $er) {
                return $er->createQueryBuilder('u')->orderBy('u.weight');
            },
        ));

        $builder->add('zip', TextFilterType::class, ['condition_pattern' => FilterOperands::STRING_STARTS]);

//        $builder->add('country', ChoiceFilterType::class, array(
//            'choices' => array_flip(Intl::getRegionBundle()->getCountryNames())
//        ));

        $builder->add('country', TextFilterType::class);

        $builder->add('email', TextFilterType::class);

        $builder->add('status', ChoiceFilterType::class, array(
            'choices' => array(
                'label.status.blocked' => 'accountLocked',
                'label.status.expired' => 'accountExpired',
                'label.status.wait' => 'notEnabled',
                'label.status.standard' => 'user',
                'label.status.certified' => 'certified',
            ),
            'placeholder' => $defaultPlaceholder,
            'apply_filter' => function (ORMQuery $filterQuery, $field, $values) {
                if (empty($values['value'])) {
                    return false;
                }

                $builder = $filterQuery->getQueryBuilder();
                switch ($values['value']) {
                    case 'accountLocked':
                        $builder->where('u.accountNonLocked = false');
                        break;
                    case 'accountExpired':
                        $builder->where('u.accountNonExpired = false');
                        break;
                    case 'notEnabled':
                        $builder->where('u.enabled = false');
                        break;
                    case 'user':
                        $builder->where('u.enabled = true')
                            ->andWhere('u.role = :role')
                            ->setParameter('role', 'ROLE_USER');
                        break;
                    case 'certified':
                        $builder->where('u.enabled = true')
                            ->andWhere('u.role = :role')
                            ->setParameter('role', 'ROLE_USER_CERTIFIED');
                        break;
                    default:
                        break;
                }

                return true;
            },
        ));
    }
}
