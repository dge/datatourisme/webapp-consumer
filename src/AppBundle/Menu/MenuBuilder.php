<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Menu;

use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

/**
 * Class MenuBuilder.
 */
class MenuBuilder
{
    /**
     * @var FactoryInterface
     */
    private $factory;

    /**
     * @var AuthorizationChecker
     */
    private $authorizationChecker;

    /**
     * @param FactoryInterface     $factory
     * @param AuthorizationChecker $authorizationChecker
     *
     * Add any other dependency you need
     */
    public function __construct(FactoryInterface $factory, AuthorizationChecker $authorizationChecker)
    {
        $this->factory = $factory;
        $this->authorizationChecker = $authorizationChecker;
    }

    /**
     * @param array $options
     *
     * @return ItemInterface
     */
    public function createMainMenu(array $options)
    {
        $menu = $this->factory->createItem('root');

        $menu->addChild('label.homepage', array(
            'route' => 'homepage',
            'extras' => array(
                'icon' => 'fa fa-home',
            ),
        ));

        if ($this->authorizationChecker->isGranted('flux.see')) {
            $menu->addChild('label.flux_plural', array(
                'route' => 'flux.index',
                'extras' => array(
                    'uri_prefix' => '/flux',
                    'icon' => 'fa fa-feed',
                ),
            ));
        }

        if ($this->authorizationChecker->isGranted('application.see')) {
            $menu->addChild('label.applications', array(
                'route' => 'application.index',
                'extras' => array(
                    'uri_prefix' => '/application',
                    'icon' => 'fa fa-mobile',
                ),
            ));
        }

        $menu->addChild('label.support', array(
            'uri' => 'https://support.datatourisme.gouv.fr',
            'linkAttributes' => array('target' => '_blank'),
            'extras' => array(
                'icon' => 'fa fa-question-circle'
            ),
        ));

        $menu->addChild('label.datagouv', array(
            'uri' => 'https://www.data.gouv.fr/fr/datasets/datatourisme-la-base-nationale-des-donnees-du-tourisme-en-open-data/',
            'linkAttributes' => array('target' => '_blank'),
            'extras' => array(
                'icon' => 'fa fa-cloud-download'
            ),
        ));

        // Administration
        if ($this->authorizationChecker->isGranted('ROLE_ADMIN')) {
            $admin = $menu->addChild('label.administration', array(
                'uri' => '#',
                'extras' => array(
                    'uri_prefix' => '/admin',
                    'icon' => 'fa fa-cogs',
                ),
            ));
            $admin->addChild('label.users', array('route' => 'user.index'));
        }

        return $menu;
    }
}
