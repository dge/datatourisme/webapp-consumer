<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Utils;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use AppBundle\Entity\User\User;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

/**
 * Class DataGouvApi.
 */
class DataGouvApi
{
    /**
     * @var string
     */
    protected $dataGouvUrl;

    /**
     * @var TokenStorage
     */
    protected $tokenStorage;

    /**
     * @var Client
     */
    protected $client;

    /**
     * DataGouvApi constructor.
     *
     * @param $dataGouvUrl
     * @param TokenStorage $tokenStorage
     */
    public function __construct($dataGouvUrl, TokenStorage $tokenStorage)
    {
        $this->dataGouvUrl = $dataGouvUrl;
        $this->tokenStorage = $tokenStorage;
        $this->client = new Client();
    }

    /**
     * @param string|null $apiKey
     *
     * @return mixed
     */
    public function me($apiKey = null)
    {
        return $this->getRequestResponse('/me/', 'GET', null, $apiKey);
    }

    /**
     * @return mixed
     */
    public function datasets()
    {
        return $this->getRequestResponse('/me/datasets/');
    }

    /**
     * @param $dataset
     * @param $filePath
     *
     * @return mixed
     */
    public function upload($dataset, $filePath)
    {
        return $this->getRequestResponse("/datasets/$dataset/upload/", 'POST', $filePath);
    }

    /**
     * @param $dataset
     *
     * @return mixed
     */
    public function getDataSetResources($dataset)
    {
        $response = $this->getRequestResponse("/datasets/$dataset/");
        if (-1 !== $response) {
            return $response->resources;
        }

        return -1;
    }

    /**
     * @param $dataset
     */
    public function clean($dataset)
    {
        $resources = $this->getDataSetResources($dataset);
        if (-1 !== $resources) {
            foreach ($resources as $resource) {
                $this->getRequestResponse("/datasets/$dataset/resources/".$resource->id.'/', 'DELETE');
            }
        }
    }

    /**
     * @param string|null $apiKey
     *
     * @return string
     */
    protected function getApiKey($apiKey = null)
    {
        if ($apiKey) {
            return $apiKey;
        }

        /** @var User $user */
        $user = $this->tokenStorage->getToken()->getUser();
        if ($user instanceof User) {
            return $user->getDataGouvApiKey();
        }

        return '';
    }

    /**
     * Do the request to the API and return the result as an array.
     *
     * @param $url
     * @param string      $method
     * @param string|null $filePath
     * @param string|null $apiKey
     *
     * @return mixed
     */
    protected function getRequestResponse($url, $method = 'GET', $filePath = null, $apiKey = null)
    {
        if (empty($this->getApiKey($apiKey))) {
            return -1;
        }

        try {
            $headers = ['X-API-KEY' => $this->getApiKey($apiKey)];
            $options = array();

            if ($filePath) {
                $options['multipart'] = array('file' => array('name' => 'file', 'contents' => fopen($filePath, 'r')));
            } else {
                $headers['Content-type'] = 'application/json';
            }
            $options = array_merge(array('headers' => $headers), $options);

            /** @var Response $response */
            $response = $this->client->request($method, $this->dataGouvUrl.$url, $options);

            return json_decode($response->getBody()->getContents());
        } catch (RequestException $e) {
            return -1;
        }
    }
}
