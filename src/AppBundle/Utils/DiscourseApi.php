<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

// use Symfony\Component\HttpClient\HttpClient;
namespace AppBundle\Utils;

use GuzzleHttp\Client;

/**
 * 
 */
class DiscourseApi
{
    private $url;

    /**
     * @var Client
     */
    private $httpClient;

    function __construct($url, $apiKey)
    {
        $this->url = $url;
        $this->httpClient = new Client(['headers' => [
            //'Api-Username' =>  'conjecto',
            //'Api-Key' => $apiKey
        ]]);
    }

    public function get($path)
    {
        $response = $this->httpClient->request('GET', $this->url . '/' . $path, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-type' => 'application/json'
            ]
        ]); 
        return json_decode($response->getBody());
    }

    /**
     * getPosts
     */
    function getPosts()
    {
        return $this->get("posts.json");
    }

    /**
     * getLatest
     */
    function getLatestTopics()
    {
        return $this->get("latest.json");
    }

    /**
     * getLatest
     */
    function getCategoryLatestTopics($id)
    {
        return $this->get("/c/$id.json");
    }

    /**
     * getTopic
     */
    function getTopic($id)
    {
        return $this->get("/t/$id.json");
    }

    /**
     * getCategories
     */
    function getCategories()
    {
        return $this->get("categories.json");
    }

    /**
     * getSite
     */
    function getSite()
    {
        return $this->get("site.json");
    }

    /**
     * getTopicUrl
     */
    function getTopicUrl($topic)
    {
        return $this->url . '/t/' . $topic->slug . '/' . $topic->id;
    }

}