<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Utils;

use AppBundle\Ontology\OntologyFactory;

/**
 * FluxWizardHelper
 */
class FluxWizardHelper
{
    /**
     * @var 
     */
    private $ontology;

    /**
     * Helper constructor.
     *
     * @param $dataGouvUrl
     * @param TokenStorage $tokenStorage
     */
    public function __construct(OntologyFactory $ontologyFactory)
    {
        $this->ontology = $ontologyFactory->create();
    }

    /**
     * Create query data object based on types & locations
     */
    public function createQueryData($types, $locations) {
        $types = $this->normalizeTypes($types);
        if (count($types)) {
            $data['types'] = $types;
        }

        $ast = $this->createLocationAst($locations);
        if ($ast) {
            $data['ast'] = ['or' => [['and' => [['thesaurus' => ['path' => $ast['path'], 'value' => $ast['values']]]]]]];
        }
        
        return $data;
    }

    /**
     * Create a SPARQL CONSTRUCT query based on types & locations
     */
    public function createConstructQuery($types, $locations) {
        $where = $this->createWhereClause($types, $locations);
        return <<<EOT
CONSTRUCT {
    ?res <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <urn:resource>. 
} WHERE {
    {$where}
}
EOT;
    }

    /**
     * Create a SPARQL SELECT COUNT query based on types & locations
     */
    public function createCountQuery($types, $locations) {
        $where = $this->createWhereClause($types, $locations);
        return <<<EOT
SELECT (count(distinct ?res) as ?count) WHERE { 
    {$where} 
}
EOT;
    }

    /**
     * Create WHERE clause
     */
    private function createWhereClause($types, $locations) {
        $types = $this->normalizeTypes($types);
        $ast = $this->createLocationAst($locations);

        $where = ["?res <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <{$this->ontology->expand(":PointOfInterest")}>"];
        $values = [];

        // types
        if ($types) {
            $where[] = "?res <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> ?type";
            $values['?type'] = $types;
        }

        // ast
        if ($ast) {
            $sbj = "res";
            foreach($ast['path'] as $path) {
                $obj = substr(md5($sbj.$path), 0, 8);
                $where[] = "?{$sbj} <{$path}> ?{$obj}";
                $sbj = $obj;
            }
            $values['?' . $sbj] = $ast['values'];
        }

        array_walk($values, function(&$value, $key) {
            $value = "VALUES $key {\n        <". join(">\n        <", $value).">\n    }";
        });

        $where = $where ? join(".\n    ", $where)."." : "";
        $values =  $values ? "\n    " . join("\n    ", $values) : "";
        
        return $where.$values;
    }

    /**
     * Normalize types selection : regroup, expand uris, etc.
     */
    private function normalizeTypes($types) {
        $classes = $this->ontology->getSubClasses(":PointOfInterest");
        foreach($classes as $uri => $class) {
            // if all secoundary types are selected, select the main one
            $sub = $class['subClasses'];
            $whole = count(array_intersect($sub, $types)) == count($sub);
            if ($whole) {
                $types = array_values(array_diff($types, $sub));
                $types[] = $uri;
            }
        }

        // if all main types are selected, unselect all to avoid useless query
        $keys = array_keys($classes);
        $whole = count(array_intersect($keys, $types)) == count($keys);
        if ($whole) {
            return [];
        }

        return $this->ontology->expand($types);
    }

    /**
     * createLocationsAst : regroup, expand uris, etc.
     */
    private function createLocationAst($locations) {
        $path = [':isLocatedAt', 'schema:address', ':hasAddressCity', ':isPartOfDepartment'];
        $values = $locations;

        if (!count($values)) {
            return null;
        }

        $individuals = $this->ontology->getIndividuals(":Region");
        $regions = [];
        $departments = $locations;

        foreach(array_keys($individuals) as $uri) {
            $dpts = $this->ontology->getProperty($uri, 'skos:narrower');
            $whole = count(array_intersect($dpts, $locations)) == count($dpts);
            if ($whole) {
                $departments = array_values(array_diff($departments, $dpts));
                $regions[] = $uri;
            }
        }

        // if all regions are selected, unselect all to avoid useless query
        $keys = array_keys($individuals);
        $whole = count(array_intersect($keys, $regions)) == count($keys);
        if ($whole) {
            return null;
        }

        // if there is no department left, search for regions
        if (!count($departments) && count($regions)) {
            $path[] = ':isPartOfRegion';
            $values = $regions;
        } 

        return [
            'path' => $this->ontology->expand($path),
            'values' => $this->ontology->expand($values)
        ];
    }

}