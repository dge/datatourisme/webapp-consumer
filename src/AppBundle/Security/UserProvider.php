<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Security;

use AppBundle\Entity\User\User;
use Datatourisme\Bundle\WebAppBundle\Security\AbstractUserProvider;

/**
 * Class UserProvider.
 */
class UserProvider extends AbstractUserProvider
{
    /**
     * @return string
     */
    protected function getUserClass()
    {
        return User::class;
    }
}
