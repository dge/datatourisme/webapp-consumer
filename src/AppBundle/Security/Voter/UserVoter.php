<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Security\Voter;

use AppBundle\Entity\User\User;
use Datatourisme\Bundle\WebAppBundle\Security\AbstractVoter;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

/**
 * Created by PhpStorm.
 * User: blaise
 * Date: 06/03/17
 * Time: 13:01.
 */
class UserVoter extends AbstractVoter
{
    const EDIT = 'user.edit';
    const DATAGOUV = 'user.datagouv';
    const ADMINISTRATE = 'user.administrate';

    /**
     * {@inheritdoc}
     */
    protected function supports($attribute, $subject): bool
    {
        if (parent::supports($attribute, $subject)) {
            return null === $subject || $subject instanceof User;
        }

        return false;
    }

    /**
     * {@inheritdoc}
     */
    protected function voteOnAttribute($attribute, $account, TokenInterface $token)
    {
        $user = $token->getUser();
        if (!$user instanceof User || (null !== $account && !$account instanceof User)) {
            // the user must be logged in; if not, deny access
            return false;
        }

        switch ($attribute) {
            case self::EDIT:
                return $this->canEdit($account, $user);
            case self::DATAGOUV:
                return $this->canDataGouv($account, $user);
            case self::ADMINISTRATE:
                return $this->canAdministrate($account, $user);
        }

        throw new \LogicException('This code should not be reached!');
    }

    /**
     * @param User $account
     * @param User $user
     *
     * @return bool
     */
    private function canEdit(User $account = null, User $user)
    {
        // only user himself or admin can edit
        if ($this->hasRole('ROLE_ADMIN', $user)) {
            return true;
        }

        return $account->getId() == $user->getId();
    }

    /**
     * @param User $account
     * @param User $user
     *
     * @return bool
     */
    private function canDataGouv(User $account = null, User $user)
    {
        if ($account && $account->getId() === $user->getId() && $account->isRole('ROLE_USER_CERTIFIED')) {
            return true;
        }

        return false;
    }

    /**
     * @param User $account
     * @param User $user
     *
     * @return bool
     */
    private function canAdministrate(User $account = null, User $user)
    {
        // cannot administrate other if not admin
        if (!$this->hasRole('ROLE_ADMIN', $user)) {
            return $account && $account->getId() === $user->getId();
        } else {
            // only superadmin can administrate admin
            if ($account && $this->hasRole('ROLE_ADMIN', $account) && !$this->hasRole('ROLE_SUPER_ADMIN', $user)) {
                return false;
            }

            return !$account || $account->getId() !== $user->getId();
        }
    }
}
