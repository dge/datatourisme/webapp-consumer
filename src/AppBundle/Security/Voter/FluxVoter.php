<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Security\Voter;

use AppBundle\Entity\Flux\Flux;
use AppBundle\Entity\User\User;
use Datatourisme\Bundle\WebAppBundle\Security\AbstractVoter;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class FluxVoter extends AbstractVoter
{
    const SEE = 'flux.see';
    const CREATE = 'flux.create';
    const EDIT = 'flux.edit';
    const DELETE = 'flux.delete';
    const DOWNLOAD = 'flux.download';
    const DATAGOUV = 'flux.datagouv';

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * {@inheritdoc}
     */
    protected function supports($attribute, $subject): bool
    {
        if (parent::supports($attribute, $subject)) {
            return null === $subject || $subject instanceof Flux;
        }

        return false;
    }

    /**
     * {@inheritdoc}
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        /** @var User $account */
        $account = $token->getUser();
        switch ($attribute) {
            case self::SEE:
                return $this->canSee($account, $subject);
            case self::CREATE:
                return $this->canCreate($account);
            case self::EDIT:
                return $this->canEdit($account, $subject);
            case self::DELETE:
                return $this->canDelete($account, $subject);
            case self::DOWNLOAD:
                return $this->canDownload($account, $subject);
            case self::DATAGOUV:
                return $this->canDatagouv($account, $subject);
        }

        throw new \LogicException('This code should not be reached!');
    }

    /**
     * @param User      $account
     * @param Flux|null $flux
     *
     * @return bool
     */
    private function canSee(User $account, $flux)
    {
        if ($flux) {
            if ($flux->getUser()->getId() === $account->getId()) {
                return true;
            } elseif ($this->hasRole('ROLE_ADMIN', $account)) {
                return true;
            }

            return false;
        }

        return true;
    }

    /**
     * @param User $account
     *
     * @return bool
     */
    private function canCreate(User $account)
    {
        if ($this->hasRole('ROLE_ADMIN', $account)) {
            return false;
        }

        return true;
    }

    /**
     * @param User $account
     * @param Flux $flux
     *
     * @return bool
     */
    private function canEdit(User $account, $flux)
    {
        if ($flux->getUser()->getId() === $account->getId()) {
            return true;
        }

        return false;
    }

    /**
     * @param User $account
     * @param Flux $flux
     *
     * @return bool
     */
    private function canDelete(User $account, $flux)
    {
        return $this->canEdit($account, $flux);
    }

    /**
     * @param User $account
     * @param Flux $flux
     *
     * @return bool
     */
    private function canDownload($account, $flux)
    {
        if ($account && $account instanceof User) {
            if ($this->hasRole('ROLE_ADMIN', $account)) {
                return true;
            }

            if ($flux->getUser()->getId() === $account->getId() && $account->isLoginable()) {
                return !empty($flux->getQuery());
            }
        } elseif ((null === $account || is_string($account)) && $flux->getUser()->isLoginable()) {
            return true;
        }

        return false;
    }

    /**
     * @param User $account
     * @param Flux $subject
     *
     * @return bool
     */
    private function canDatagouv($account, $subject)
    {
        if (null == $subject || !($account instanceof User)) {
            return false;
        }

        if ($account->getId() != $subject->getUser()->getId()) {
            return false;
        }

        if ($account->isRole('ROLE_USER_CERTIFIED') && $subject->isDataGouv() &&
            !empty($account->getDataGouvApiKey() && !empty($subject->getDataSet()))) {
            return true;
        }

        return false;
    }
}
