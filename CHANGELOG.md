CHANGELOG
===================

Ce fichier est basé sur [Keep a Changelog](http://keepachangelog.com/)
et le projet utilise [Semantic Versioning](http://semver.org/).

## [1.7.2] - 2022-01-26

- Mise en place de la commande d'expiration des comptes utilisateurs

### Modifications

- Mise à jour du template public
## [1.7.1] - 2020-12-07

### Modifications

- Mise à jour du template public

## [1.7.0] - 2020-10-15

### Ajout

- Nouvel assistant de création de flux

## [1.6.1] - 2020-07-02

### Modifications

- Flux : préselection du format par défaut dans le champs Format initial

## [1.6.0] - 2020-06-24
### Ajout

- Ajout des format bundle JSON et XML
- Mise à jour des dépendances : visual-query-editor
- Refactor des pages d'aide CONSTRUCT et SELECT

## [1.5.5] - 2020-02-25

### Modifications

- Ajout d'un timeout de 2 minutes sur l'éditeur de requête visuelle

## [1.5.4] - 2020-01-30

### Modifications

- Mise à jour des dépendances : visual-query-editor

## [1.5.3] - 2020-01-28

### Modifications

- Mise à jour des dépendances : visual-query-editor et datatourisme/api

## [1.5.2] - 2020-01-10

### Ajout

- Ajout d'une notification de bienvenu présentant l'ensemble des outils disponibles à l'utilisateur

## [1.5.0] - 2019-12-12

### Ajout

- Dashboard d'accueil avec accès à discourse
- Commandes de nettoyage
- Les application peuvent maintenant être supprimées

## [1.4.3] - 2019-10-01

### Modifications

- Mise à jour de la dépendance webapp-bundle
- Mise à jour de l'adresse datatourisme.fr vers https://info.datatourisme.gouv.fr

## [1.4.2] - 2019-01-23

### Correction

- La table de prévisualisation des résultats de requête est maintenant responsive

## [1.4.1] - 2019-01-15

### Modifications

- Mise à jour de la dépendance webapp-bundle

### Ajout

- Ajout du lien vers l'espace support

## [1.4.0] - 2019-01-14

### Modifications

- Mise à jour des IRI des ontologies DATAtourisme

### Ajout

- Ajout de commandes de maintenance

## [1.3.5] - 2018-11-14

### Ajout

- Ajout d'une route publique vers les CGU (/fr/cgu.pdf)

## [1.3.4] - 2018-10-30

### Fixes

- Ajout d'une exception de sécurité pour l'api d'ontologie
- Un flux data.gouv.fr n'est jamais désactivé

## [1.3.3] - 2018-09-25

### Ajout

- Ajout du support de l'authentification SSO pour Discourse

### Modifications

- Mise à jour des dépendances composer

## [1.3.2] - 2018-07-13

### Fixes

- Hotfixes

## [1.3.1] - 2018-07-12

### Ajout

- Ajout de la version EN des CGU

## [1.3.0] - 2018-06-20

### Ajout

- Traduction intégrale de l'application en anglais

## [1.2.2] - 2018-06-07

### Modifications

- Les langues non disponibles sont grisées dans le formulaire de flux

## [1.2.1] - 2018-06-04

### Ajout

- La liste des langues est maintenant disponible dans la configuration
d'un flux
- La liste des langues est transmise au worker

## [1.2.0] - 2018-05-16

### Ajout

- Les applications sont maintenant éditables
- Export des comptes diffuseurs en CSV

### Modifications

- Suppression de l'entité Organization
- OrganizationType devient UserType
- Suppression de champs (fonction, téléphone, adresse)
- Ajout de champs (Code postal, Pays)
- Suppression du mode DEMO

## [1.1.3] - 2018-05-10

### Modifications
- Mise à jour de la dépendance datatourisme-webappbundle

## [1.1.2] - 2018-03-28

### Modifications
- Mise à jour de la dépendance "visual query editor"

## [1.1.1] - 2018-03-23

### Modifications
- L'utilisateur peut maintenant télécharger le dernier flux généré sans avoir à reprogrammer la génération du flux

### Correction
- Erreur 500 lors de l'utilisation du filtre Utilisateur dans la liste des flux

## [1.1.0] - 2018-03-16

### Ajout
- Ajout du format HDT

### Correction
- Affichage correct des erreurs

## [1.0.8] - 2018-03-14

### Ajout
- Ajout du droit de substitution d'authentification pour ROLE_SUPER_ADMIN

## [1.0.7] - 2018-03-13

### Correction
- Mise à jour de semgraphql (1.1.7)

## [1.0.6] - 2018-03-12

### Ajout
- Lien vers la liste des flux sur le profil utilisateur

### Correction
- Hotfix : mise à jour de semgraphql

## [1.0.5] - 2018-03-01

### Correction
- Hotfix : mise à jour de semgraphql

## [1.0.4] - 2018-03-01

### Correction
- Hotfix : le cache de graphQL est maintenant stocké dans le dossier cache de Symfony

## [1.0.3] - 2018-03-01

### Modifications
- Symfony 3.4
- webapp-bundle 1.0

## [1.0.2] - 2018-03-01

### Modifications
- Mise à jour de la dépendance semgraphql

## [1.0.1] - 2018-02-07

### Corrections
- Hotfix : Gérérer -> Générer

## [1.0.0] - 2018-01-18

### Ajouts
- Rôles / droits
    - Implémentation du rôle "Super administrateur"
    - Implémentation du rôle "Administrateur"
    - Implémentation du rôle "Réutilisateur"
    - Implémentation du rôle "Réutilisateur validé"
    - Implémentation du rôle "Réutilisateur certifié"
- Authentification
    - Procédure d'enregistrement
    - Procédure d'authentification
    - Procédure de récupération de mot de passe
    - Procédure d'acceptation des conditions d'utilisation
    - Procédure de déconnexion
- Gestion du profil
    - Modification du profil utilisateur
    - Modification des informations de la société
    - Renseignement de la clé API data.gouv.fr
- Administration des utilisateurs
    - Liste des utilisateurs
    - Modification des utilisateurs
    - Blocage d'un utilisateur
    - Validation d'un utilisateur
    - Certification d'un utilisateur
    - Création d'un nouvel administrateur
    - Suppression d'un utilisateur
- Application
    - Liste des applications
    - Ajout d'une application
    - Génération de la clé API
    - Consultation de la clé API
- Flux
    - Liste des flux
    - Ajout d'un flux
    - Modification d'un flux
    - Liste des téléchargements des données du flux
    - Sélection du jeu de donnée data.gouv.fr pour exporter les données du flux
    - Editeur expert pour les requêtes SPARQL
    - Export CSV des téléchargements pour les administrateurs
    - Téléchargement manuel des données du flux
    - Téléchargement des données du flux via le webservice
    - Demander la première génération des données du flux
- Tests Behat/Mink pour l'ensemble des fonctionnalités
- Ajout d'une notification aux administrateurs lors de l'inscription d'un utilisateur

### Modifications
- Ajouts de nouveaux types d'organisations
- Le choix d'un type d'entreprise intervient maintenant à l'inscription, est est facultative
- Plus d'indication d'adresse mail utilisée lors de l'inscription
