imports:
    - { resource: parameters.yml }
    - { resource: security.yml }
    - { resource: services.yml }

# Put parameters here that don't need to change on each machine where the app is deployed
# https://symfony.com/doc/current/best_practices/configuration.html#application-related-configuration
parameters:
    locale: fr
    locales: [fr, en]
    timezone: "Europe/Paris"

framework:
    #esi: ~
    translator: { fallbacks: ['%locale%'] }
    secret: '%secret%'
    router:
        resource: '%kernel.project_dir%/app/config/routing.yml'
        strict_requirements: ~
    form: ~
    csrf_protection: ~
    validation: { enable_annotations: true }
    #serializer: { enable_annotations: true }
    templating:
        engines: ['twig']
    default_locale: '%locale%'
    trusted_hosts: ~
    session:
        # https://symfony.com/doc/current/reference/configuration/framework.html#handler-id
        handler_id: session.handler.native_file
        name: "3660aed199fe23f512e74c8a42ef05d2"
        save_path: '%kernel.project_dir%/var/sessions/%kernel.environment%'
    fragments: ~
    http_method_override: true
    assets:
        version_strategy: 'Datatourisme\Bundle\WebAppBundle\Asset\VersionStrategy\GulpBusterVersionStrategy'
    php_errors:
        log: true

# Fixtures
hautelook_alice:
    db_drivers:
        orm: ~          # Enable Doctrine ORM if is registered
    locale: fr_FR       # Locale to used for faker; must be a valid Faker locale otherwise will fallback to en_US
    seed: 1             # A seed to make sure faker generates data consistently across runs, set to null to disable
    persist_once: false # Only persist objects once if multiple files are passed
    loading_limit: 5    # Maximum number of time the loader will try to load the files passed

# Twig Configuration
twig:
    debug: '%kernel.debug%'
    strict_variables: '%kernel.debug%'
    form_themes:
        - DatatourismeWebAppBundle:Form:bootstrap_3_horizontal_layout.html.twig
        - DatatourismeWebAppBundle:Form:fields.html.twig
        - LexikFormFilterBundle:Form:form_div_layout.html.twig
        - form/fields.html.twig
    globals:
        app_name: "%app_name%"
        app_locales: %locales%
    date:
        timezone: "%timezone%"

# Doctrine Configuration
doctrine:
    dbal:
        server_version: 9.6
        driver: pdo_pgsql
        host: '%database_host%'
        port: '%database_port%'
        dbname: '%database_name%'
        user: '%database_user%'
        password: '%database_password%'
        charset: UTF8
        types:
            uuid: Ramsey\Uuid\Doctrine\UuidType
        # if using pdo_sqlite as your database driver:
        #   1. add the path in parameters.yml
        #     e.g. database_path: "%kernel.project_dir%/var/data/data.sqlite"
        #   2. Uncomment database_path in parameters.yml.dist
        #   3. Uncomment next line:
        #path: '%database_path%'

    orm:
        auto_generate_proxy_classes: '%kernel.debug%'
        naming_strategy: doctrine.orm.naming_strategy.underscore
        auto_mapping: true

# Swiftmailer Configuration
swiftmailer:
    transport: "%mailer_transport%"
    host:      "%mailer_host%"
    port:      "%mailer_port%"
    username:  "%mailer_user%"
    password:  "%mailer_password%"
    spool:     { type: memory }
    auth_mode:  "%mailer_auth_mode%"
    encryption: "%mailer_encryption%"

# JMS i18n locale
jms_i18n_routing:
    default_locale: %locale%
    locales: %locales%
    strategy: prefix

# Datatourisme WebApp
datatourisme_web_app:
    # mail templates
    mailer:
        sender:
            name: "%app_name%"
            address: "%sender_from%"
        subject_template: "[%app_name%] %s"
        emails:
            account.register:
                template: email/account/register.html.twig
            account.reset_password:
                template: email/account/reset_password.html.twig
            account.welcome:
                template: email/account/welcome.html.twig
            account.certify:
                template: email/account/certify.html.twig
            account.block:
                template: email/account/block.html.twig
            account.delete:
                template: email/account/delete.html.twig
            account.expire:
                template: email/account/expire.html.twig
            account.expire_delete:
                template: email/account/expire_delete.html.twig
