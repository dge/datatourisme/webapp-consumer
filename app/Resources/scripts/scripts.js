/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';

window.jQuery = window.$ = require('jquery');
var angular = require("angular");

/** datatourisme front framework **/
require('datatourisme-webapp-bundle');

/** components **/
require('./components/code-mirror');
require('./components/sparql/expert-editor/expert-editor');

/** apps **/
require('./ng-app/visual-query-editor');
//require('./ng-app/ruleset-wizard/ruleset-wizard');