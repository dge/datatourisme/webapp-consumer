/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
"use strict";

var $ = require('jquery');
var SparqlEditor = require('./SparqlEditor/SparqlEditor');

$.fn.initComponents.add(function(dom) {
    $("[data-sparql-editor]", dom).each(function() {
        var textarea = $(this);
        var submitEl = $("[data-query-preview-submit]", dom);
        var frameEl  = $("[data-query-preview-frame]", dom);
        var endpoint = submitEl.data("queryPreviewSubmit");
        var options  = textarea.data('sparqlEditor');
        var dfd = jQuery.Deferred();

        $
            .ajax({
                method: "GET",
                url: "/fr/api/ontology/lite",
                cache: true
            })
            .done(function(ontology) {
                var sparqlEditor = new SparqlEditor(ontology, textarea, options);
                sparqlEditor.timeout(200);
                sparqlEditor.watchForPreview(endpoint, submitEl, frameEl);
                sparqlEditor.editor.setSize("100%");

                var prefixes = $("[data-sparql-prefixes]", dom).data("sparqlPrefixes");
                if (typeof prefixes === "object")
                    sparqlEditor.editor.addPrefixes(prefixes);

                dfd.resolve(sparqlEditor.editor);
            })
        ;

        textarea.data('promise', dfd.promise());
    })
});