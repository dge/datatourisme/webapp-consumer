### 1. Introduction

1\. The Directorate General for Enterprise (DGE) publishes and develops www.datatourisme.gouv.fr, a national platform for the collection, standardisation and open data distribution of tourism data produced and distributed by tourism stakeholders in the public sector, i.e. tourist information centres, departmental tourism agencies and commissions, and regional tourism commissions.

2\. The www.datatourisme.gouv.fr platform allows reusers to use tourism datasets freely in accordance with the Etalab licence ouverte/open licence.

### 2. Definitions

3\. The terms defined hereafter will have the following meanings:

* “Administrator” means the person(s) authorised by the DGE to ensure the proper operation of the platform.
* “Terms” means these terms of use of the platform.
* “DGE” means the Directorate General for Enterprise.
* “Distributor” means the reuser of datasets published on the platform.
* “Dataset” means the tourism data or sites (“tourism points of interest”, or “POI”) that are aggregated, stored and published on the platform.
* “Platform” means the DATAtourisme platform provided by the DGE and accessible only to distributors at the following address: https://diffuseur.datatourisme.gouv.fr.
* “Accessories” means the services and material provided by the DGE through the platform, such as the toolbox and the API.
* “Producer” means any individual or legal entity authorised by the DGE to upload datasets to the platform. Producers include tourist information centres, departmental tourism agencies and commissions, and regional tourism commissions.

### 3. Requirements

4\. To access the platform, distributors must:

* use appropriate IT equipment
* use a valid e-mail address
* use a compatible web browser
* accept the use of session cookies and JavaScript

5\. Distributors agree to these terms and hereby state that:

* they have read the terms of use of the platform
* they have received all the information required to make an informed decision about whether the platform fulfils their expectations, objectives and needs
* they have all the technical skills required to access and use the platform

### 4. Purpose

6\. These terms set out the terms and conditions governing the use of the platform by distributors.

### 5. Effective date – Period of use

7\. These terms will apply throughout the entire period of use of the platform by distributors, i.e. from the date their account is activated until it is deactivated.

### 6. Acceptance and applicability

8\. Distributors agree to be bound by these terms after clicking on “I agree” when first connecting to the platform. The terms are then automatically archived in each distributor’s personal account.

9\. These terms govern the use of both the platform and the accessories.

10\. In all circumstances, by using the platform for the first time, distributors automatically acknowledge that they have read and unreservedly agreed to the terms of use.

11\. The DGE may change or amend the terms at any time if the platform or its features are updated. In this case, users will be notified by e-mail.

12\. If the terms change, distributors will be notified of the new terms when they next connect to the platform.

13\. Distributors’ use of the platform following any such change constitutes their agreement to the new terms.

14\. The terms available on the platform prevail over any other previous printed versions.

### 7. Availability

15\. The platform can be accessed 24/7 unless it is under maintenance or experiencing technical issues.

16\. La DGE se réserve le droit, sans préavis, ni indemnité, de fermer temporairement ou définitivement l’accès à la plateforme en cas de risque pour la sécurité de cette dernière ou de menace pour l’intégrité des jeux de données.

### 8. Use of the platform

17\. Use of the platform by distributors is strictly personal.

18\. Each distributor’s personal account is non-transferable and cannot be shared with other distributors under any circumstances.

19\. Unless distributors have sent a request to the DGE for their account to be suspended or deleted or they have made an objection to the DGE, they are responsible for all use of the platform.

20\. Distributors are solely responsible for the use of their personal account.

21\. Distributors agree to use the platform only in accordance with the terms defined herein and to:

* not use the platform for personal advertising purposes or for selling products
* not commit any act of infringement or reproduce, download, represent or modify all or part of the platform
* not access and/or use the platform as an automated data processing system. Any fraudulent access to or use of the platform is prohibited and amounts to a criminal offence. The same applies to any attempt to impede or disrupt the operation of the system or to add, delete or modify the data contained on the platform
* not interfere with the proper operation of the platform and not alter, transform or modify the accessible data published on the platform. Distributors also agree in particular not to introduce any viruses or other material which is technologically harmful to the platform, data or services available on the platform
* not harm the Administration’s reputation

22\. Distributors accept complete responsibility for their use of the platform. They agree to use the platform fairly and to comply with these terms and all applicable laws and regulations, in particular intellectual property rights.

23\. Distributors must not under any circumstances upload, store, publish or transmit any files or messages with offensive, racist, pornographic or defamatory material (this list is non-exhaustive). Any breach of this provision will be referred to the appropriate legal authorities.

### 9. Conditions of access to the platform

24\. Distributors need to register for an account in order to access the platform.

25\. Registration includes the following steps:

* Step 1: Distributors fill in the registration form available on the platform. They must complete all the required fields
* Step 2: Distributors register for an account by clicking on “Create my account” and check a box in order to agree to the terms
* Step 3: The DGE sends out an account confirmation e-mail and redirects distributors to a web page to change their password
* Step 4: Once they have changed their password, distributors are automatically redirected to the platform’s log-in page

26\. The platform’s full range of services is not available to distributors until the administrator approves their account.

27\. Distributors can go their personal account and change their password at any time by clicking on the “Administration” tab, then “Change my password”.

28\. Distributors accept complete responsibility for storing and using their password. They must take all necessary steps to prevent unauthorised or fraudulent use of their account.

29\. It is recommended that distributors:

* change their password regularly
* not reuse a password from another service

30\. Distributors must log off and close their browser window after using their account in order to prevent other persons from accessing their personal information. Distributors must be particularly careful when accessing their account using a Wi-Fi connection or a shared connection.

31\. If distributors notice or suspect their password has been the subject of unauthorised or fraudulent use or any security breach, they must notify the DGE immediately using the following contact methods:

* via the contact form at the following address: https://www.entreprises.gouv.fr/dge/ecrire-a-la-dge?type=DataTourisme
* or via the platform’s internal e-mail system

32\. Upon receipt of this notification, the DGE will deactivate the distributor’s account within a reasonable timeframe.

33\. The password is strictly personal and:

* must comply with the DGE’s security requirements
* must not under any circumstances be communicated to a third party

34\. **THE DGE WILL NEVER ASK DISTRIBUTORS FOR THEIR PASSWORD UNDER ANY CIRCUMSTANCES. ANY PASSWORD REQUESTS OF THIS TYPE SHOULD BE CONSIDERED FRAUDULENT.**

35\. Personal accounts are locked for one hour after five unsuccessful sign-in attempts. Administrators are also able to unlock accounts.

36\. The DGE reserves the right to change the authentication technologies and processes and to replace passwords with any other technology it deems necessary.

### 10. Notifications

37\. Distributors may receive notifications while using the platform.

38\. These notifications are designed to inform distributors of all the important events related to their activity and the flows they manage on the platform.

### 11. Security

39\. The DGE endeavours to provide a secure platform, in accordance with proper procedures, and complies with the RGS 2.0 and OWASP Top 10 – 2013 security standards, as well as the best practices defined by France’s National Cybersecurity Agency (ANSSI). TLS is used to encrypt all information exchanged between a distributor’s browser and the platform, and the TLS certificate is issued by a certificate authority designated by the DGE.

40\. Distributors acknowledge that they are familiar with their type of internet network, in particular their internet connection speeds and response times for viewing, querying or transferring data.

41\. Distributors must notify the DGE of any bugs or problems with their personal account.

42\. If a security failure is detected, the DGE will notify distributors in accordance with applicable laws. It may outline any action required. Distributors will assume the cost of such action.

43\. The DGE can implement any necessary emergency procedures to ensure the platform’s security.

44\. Distributors agree to take any appropriate action in order to protect their own data and/or software from any viruses or other material which is technologically harmful on the internet network.

45\. Whatever the circumstances, distributors accept the characteristics and limitations of the internet.

### 12. The DGE’s responsibility

46\. The DGE will not be held responsible if access to all or part of the platform is temporarily or permanently unavailable or if a response problem or any general connection issues are experienced.

47\. The DGE will not be held responsible in the event of any fraudulent or improper use of an account after a password is deliberately or accidently revealed to others.

48\. The DGE will not be held responsible for any breach of these terms by another distributor.

49\. Given the large number of datasets published on the platform by producers, the DGE will not be held responsible for any omissions, inaccuracies or inadequate updates, whether these are due to its actions or the actions of the producers providing the datasets. Checks carried out upstream by the DGE are only designed to ensure that the datasets stored on the platform are technically compatible and under no circumstances include any checks of the quality, accuracy or veracity of the datasets stored on the platform.

50\. Distributors are responsible for checking the quality and veracity of the datasets when reusing them. They are solely responsible for their reuse.

51\. Distributors acknowledge and accept that the DGE cannot be held responsible for the loss or corruption of datasets as a result of improper use of the platform or mismanagement of security by distributors.

### 13. Content reporting

52\. The DGE is committed to the fight against illegal, false and technically incompatible content. With this aim, the DGE enables distributors to notify it of any category of content published on the platform that breaches these terms or laws and regulations.

53\. While the administrators monitor content upstream, the DGE has also implemented a reporting system to monitor datasets posted by distributors on a downstream basis. This ensures that datasets published on the platform comply with the platform’s terms where possible.

54\. Any distributor who has identified illegal, false or technically incompatible content can report such content. Distributors are solely responsible for reporting the content.

55\. Any dataset reported which does not comply with the terms will be deleted by the DGE within a reasonable timeframe after it is reported.

### 14. Intellectual property

56\. Nothing in these terms will be construed as a transfer of ownership of any sort of intellectual property rights from the DGE to a distributor.

57\. The site, trademarks, drawings, models, images, text, photos, logos, graphics standards, software, search engines, database structures and domain names (this list is non-exhaustive) are the exclusive property of the DGE or its licensors.

58\. Any reproduction and/or representation of the whole or part of any of these rights without the DGE’s permission is prohibited and may constitute an infringement under Articles L. 335-2 *et seq.* of the French Intellectual Property Code.

59\. Distributors will refrain from any action or activity that may infringe directly or indirectly upon the DGE’s intellectual property rights.

60\. This intellectual property clause will remain in effect after the terms have ended.

### 15. Open data

61\. All the datasets published on the platform are governed by the Etalab licence ouverte/open licence that is in effect at the time of publication.

62\. Producers of datasets grant a worldwide, royalty-free, non-exclusive and perpetual right to freely extract and reuse the datasets published on the platform for commercial or non-commercial use.

63\. Distributors may reuse the datasets to:

* reproduce or copy them
* adapt, modify, extract or transform them in order to generate flows, products or services
* communicate, disseminate, redistribute, publish or transmit them
* use them for commercial purposes, for instance combining them with other datasets or including them in their own products or applications

64\. However, distributors must always cite the source of the dataset that is being reused and the date of the last update.

65\. For more information on the open licence and related rights, please refer to the Etalab licence ouverte/open licence at the following address: https://www.etalab.gouv.fr/wp-content/uploads/2017/04/ETALAB-Licence-Ouverte-v2.0.pdf.

### 16. Personal data

#### 16.1. The DGE’s responsibility

66\. Access to the platform is subject to a personal account being created by a distributor and requires the use of personal data by the DGE.

67\. Some of the data collected is personal data for the creation of a distributor’s personal account. This includes the company employing the distributor and the distributor’s last name, first name and e-mail address. Distributors can update their personal data at any time or edit their profile through the “My profile” interface.

68\. Data is collected with the distributor’s consent when they register for the platform.

69\. This data is for the attention of the DGE, which is responsible for data processing.

70\. To ensure the proper operation of the platform, the DGE collects and stores personal data for the following purposes:

* managing distributors’ accounts
* handling technical support requests
* handling requests for right of access, right to rectification or right to object
* publishing statistics on the platform, in particular on the interface for producers (categories of data downloaded by distributors, frequency of downloads, etc.)

71\. Personal data can be accessed by persons in certain positions authorised by the DGE and by any sub-contractors.

72\. The DGE saves and processes the data collected with the sole aim of providing distributors with full and proper use of the platform.

73\. Within a data sharing framework, the DGE may use sub-contractors to operate services available on the platform and to host data.

74\. Distributors’ personal data is stored for as long as their account is activated. Data is stored for mandatory retention periods after then in accordance with legal obligations.

75\. Distributors are informed that the DGE does not transfer any data outside of France.

76\. Distributors are informed that they have the right to access, be informed about and rectify their personal data and may, where necessary, rectify, complete, update or erase any personal data that is inaccurate, incomplete, incorrect or outdated or that is not allowed to be collected, used, communicated or stored.

77\. Distributors also have the right to give general and specific instructions regarding the retention, erasure and communication of their data after death. General instructions are to be directed to a third party, who will be appointed by decree.

78\. Distributors can exercise all of these rights by directing their requests to the DGE:

* in writing to the following address: DGE – Bureau de la communication/Communication Bureau – 67 Rue Barbès – 94200 Ivry sur Seine, France
* via the electronic contact form available at: https://www.entreprises.gouv.fr/dge/ecrire-a-la-dge?type=DataTourisme

79\. Requests for exercising rights of access must include a copy of a valid form of identification.

80\. Distributors have the right to make a claim to the Commission Nationale de l’Informatique et des Libertés (French Data Protection Authority, hereafter referred to as “CNIL”).

#### 16.2. Distributors’ responsibility

81\. The datasets published on the platform may contain personal data.

82\. In accordance with the Etalab licence ouverte/open licence, distributors are subject to applicable personal data protection regulations currently in force.

83\. Distributors make a guarantee to the DGE that they comply with applicable legal and regulatory requirements governing personal data protection.

84\. Distributors are responsible to relevant persons for any alleged breaches of personal data protection regulations arising from the reuse of datasets.

### 17. Statistics

85\. In order to improve and personalise the platform and its content and to ensure that it meets the requirements of distributors, the DGE stores the connection history of distributors, i.e. the number of visits to the platform, the number of page views, distributors’ activity on the platform and the frequency of their return visits.

86\. The categories of data published for statistical purposes are determined solely by the DGE and under no circumstances by a third party.

### 18. Agreement on evidence

87\. Data collected and saved in the DGE’s information systems will be retained in accordance with reasonable security procedures and considered evidence of communication between the parties.

88\. Electronic acceptance of terms, notifications within the platform and any contractual relationship entered into on line by distributors have the same probative value between the parties as paper documents.

89\. This provision will be considered a legally binding agreement on evidence between the parties.

### 19. Hyperlinks

90\. The platform may contain hyperlinks to third-party websites.

91\. Distributors are informed that the websites accessed through hyperlinks do not belong to the DGE.

92\. The DGE accepts no responsibility for the content on these websites accessed through hyperlinks or for the confidentiality policy of these websites.

93\. Distributors cannot hold the DGE responsible for any losses or damages of any kind as a result of clicking on a hyperlink.

### 20. Cookies

94\. The DGE is authorised to track distributors’ browsing activity on the platform and agrees to only use cookies for either session tracking (this is necessary for the platform to operate properly) or for audience measurement.

### 21. Account closure

95\. Distributors can request to unsubscribe from the platform at any time.

96\. If distributors do not log in to or use their account for two years, the administrator can close the account. Distributors will receive e-mail notification.

97\. Distributors will receive an e-mail alert prior to their account being closed.

98\. Distributors are notified of any closure.

99\. Distributors are informed that they lose access to their secure personal account as soon as they cease to be users of the platform.

100\. Deleting an account is irreversible.

101\. Closing an account for any reason results in the loss of all data, information and benefits gained through the use of the platform.

102\. Distributors are responsible for taking the necessary measures to store their data if they wish to do so.

103\. Distributors can send a request to the administrator to close their account:

* in writing to the following address: DGE – Bureau de la communication/Communication Bureau – 67 Rue Barbès – 94200 Ivry sur Seine, France
* via the electronic contact form available at the following address: https://www.entreprises.gouv.fr/dge/ecrire-a-la-dge?type=DataTourisme
* or via the platform’s internal e-mail system

### 22. Suspension and resolution

104\. If distributors breach any of these terms, the administrator reserves the right to suspend access to the platform until the reason for the suspension has been resolved, without compensation or refund, if applicable. Access will be suspended eight days after a letter is sent to distributors requesting that they comply with these terms.

105\. Any account suspensions of more than 30 days which have not been resolved by distributors will result in the deletion of the account.

106\. The DGE may close the account immediately and without notice in the event of fraudulent use for which the DGE may be liable. Distributors are notified of any account closures by e-mail.

107\. If an account is closed as a result of a distributor’s wrongdoing, they will not be able to reopen their account for at least one year. Furthermore, reactivating or reopening an account may be subject to prior agreement from the DGE.

### 23. General provisions

108\. These terms are governed by the laws of France.

109\. The section headings are to be disregarded if they are interpreted in such a way as to be in conflict with clause language.

110\. If any provision of these terms is held to be invalid or declared as such under any law or regulation or is subject to a final decision of a competent court, the remaining provisions will continue to remain in full force and effect.

111\. **THE PARTIES SUBMIT TO THE JURISDICTION OF THE PARIS COURT OF APPEAL *\[COUR D’APPEL DE PARIS\]* IN RESPECT OF ANY DISPUTES RELATING TO THESE TERMS OF USE, EXCLUDING CLAIMS INVOLVING MULTIPLE DEFENDANTS OR THIRD-PARTY PROCEEDINGS.**
