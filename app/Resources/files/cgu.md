### 1. Préambule

1\. La Direction générale des entreprises édite et développe www.datatourisme.gouv.fr, plateforme nationale de collecte, d’uniformisation et de diffusion en open data des données touristiques produites et diffusées par les acteurs institutionnels de tourisme, à savoir les offices de tourisme, les agences et comités départementaux du tourisme, ou encore les comités régionaux de tourisme.

2\. La plateforme www.datatourisme.gouv.fr offre la possibilité aux réutilisateurs d’utiliser librement des jeux de données touristiques dans le respect de la licence ouverte Etalab.

### 2. Définitions

3\. Les termes ci-dessous définis auront entre les parties la signification suivante :

* « administrateur » : désigne les personnes habilitées par la DGE pour veiller au bon fonctionnement de la plateforme ;
* « conditions générales » : désigne les présentes conditions générales d’utilisation de la plateforme ;
* « DGE » : désigne la Direction générale des entreprises ;
* « diffuseur » : désigne le ré-utilisateur de jeux de données publiés sur la plateforme ;
* « jeu de données » : désigne les données ou objets touristiques de type « Points d’intérêt touristique » (« POI ») agrégés, stockés et publiés sur la plateforme ;
* « plateforme » : désigne la plateforme DATAtourisme mise à disposition par la DGE et accessible, uniquement aux diffuseurs, à l’adresse https://diffuseur.datatourisme.gouv.fr ;
* « accessoires » : désigne les services et la documentation fournis par la DGE par l’intermédiaire de la plateforme tels que la boîte à outils et l’API ;
* « producteur » : désigne toute personne physique ou morale autorisée par la DGE à uploader les jeux de données sur la plateforme. Il s’agit notamment des offices de tourisme, les agences et comités départementaux de tourisme, ou encore les comités régionaux de tourisme.

### 3. Prérequis

4\. L’accès à la plateforme par le diffuseur est conditionné au respect des prérequis suivants :

* disposer d’un équipement informatique adapté ;
* disposer d’une adresse de courrier électronique valide ;
* disposer des navigateurs web compatible ;
* autoriser l’utilisation des cookies de session et des scripts javascripts

5\. Le diffuseur, en acceptant les présentes conditions générales, déclare :

* avoir pris connaissance des conditions dans lesquelles fonctionne la plateforme ;
* disposer de toutes les informations nécessaires pour considérer que la plateforme correspond à ses attentes, ses objectifs et ses besoins ;
* disposer de toutes les compétences techniques nécessaires pour accéder et utiliser la plateforme.

### 4. Objet

6\. Les présentes conditions générales ont pour objet de définir les conditions et modalités régissant l’utilisation de la plateforme par le diffuseur.

### 5. Entrée en vigueur – Durée

7\. Les conditions générales sont applicables pendant toute la durée de l’utilisation de la plateforme par le diffuseur, c’est-à-dire à compter de l’activation du compte jusqu’à sa désactivation.

### 6. Acceptation et opposabilité

8\. Les présentes conditions générales sont opposables au diffuseur dès leur acceptation, formalisée par un clic sur le bouton « Accepter », lors de la première connexion du diffuseur sur la plateforme, ce qui entraîne l’archivage automatique des conditions générales sur le compte personnel du diffuseur.

9\. L’acceptation des conditions générales s’applique tant pour la plateforme que pour ses accessoires.

10\. Dans tous les cas, à la date de la première utilisation de la plateforme par le diffuseur, les conditions générales sont réputées lues et acceptées sans réserve par ce dernier.

11\. Les conditions générales sont susceptibles d’être modifiées ou aménagées à tout moment par la DGE afin de faire évoluer la plateforme et ses fonctionnalités. Dans ce cas, une notification sera envoyée aux utilisateurs par email.

12\. En cas de modification, les nouvelles conditions générales sont notifiées au moment de la nouvelle connexion, par le diffuseur, à la plateforme.

13\. Toute nouvelle utilisation de la plateforme, postérieure à la notification de la modification des conditions générales, vaut acceptation par le diffuseur des nouvelles conditions générales.

14\. Les conditions générales figurant sur la plateforme prévalent sur toute version imprimée de date antérieure.

### 7. Disponibilité

15\. La plateforme est accessible 24/24 heures, 7/7 jours sous réserve des opérations de maintenance ou de difficultés ou contraintes techniques.

16\. La DGE se réserve le droit, sans préavis, ni indemnité, de fermer temporairement ou définitivement l’accès à la plateforme en cas de risque pour la sécurité de cette dernière ou de menace pour l’intégrité des jeux de données.

### 8. Utilisation de la plateforme

17\. L’utilisation de la plateforme par le diffuseur est strictement personnelle.

18\. Le compte personnel est intuitu personae c’est à dire qu’il est à la discrétion du seul diffuseur et ne peut en aucun cas être partagé avec d’autres diffuseurs.

19\. Sauf à avoir engagé préalablement une demande de suspension ou de suppression de compte ou une procédure d’opposition auprès de la DGE, tout usage de la plateforme est réputé avoir été réalisé par le diffuseur.

20\. Le compte personnel est utilisé sous la seule et unique responsabilité du diffuseur.

21\. Le diffuseur s’engage à n’utiliser la plateforme que dans les seules conditions définies aux présentes et en outre :

* à ne pas détourner l’utilisation de la plateforme à des fins personnelles publicitaires ou de vente de produits ;
* à ne commettre aucun acte de contrefaçon, à ne pas reproduire, télécharger, représenter ou modifier, tout ou partie de la plateforme ;
* à ne pas accéder et/ou se maintenir dans la plateforme entendue comme un système de traitement automatisé de données. Tout accès ou maintien frauduleux à ce dernier est interdit et sanctionné pénalement. Il en est de même pour toute entrave ou altération du fonctionnement de ce système, ou en cas d’introduction, de suppression ou de modification des données qui y sont contenues ;
* à ne pas perturber le bon fonctionnement de la plateforme, à ne pas altérer, transformer ou modifier les données accessibles et publiées sur la plateforme et notamment, à ne pas introduire de virus ou toute autre technologie nuisible à la plateforme, aux données ou aux services qui y sont proposés ;
* à ne pas nuire à l’image de l’Administration.

22\. Le diffuseur est entièrement responsable de l’utilisation qu’il fait de la plateforme. Il s’engage à l’utiliser de façon loyale, dans le respect des présentes conditions générales, des lois et règlements applicables, notamment ceux relatifs à la propriété intellectuelle.

23\. Le diffuseur ne doit, en aucune manière, se livrer au chargement, au stockage, à la publication ou à la diffusion de fichiers et de messages dont le contenu présente un caractère injurieux, raciste, pornographique ou diffamatoire, sans que cette liste ne soit exhaustive. En cas de violation de ce point, les comportements litigieux seront signalés au tribunal compétent.

### 9. Condition d’accès à la plateforme

24\. Pour accéder à la plateforme, l’ouverture d’un compte par le diffuseur est nécessaire.

25\. L’inscription à la plateforme comprend les étapes suivantes :

* étape 1 : le diffuseur complète un formulaire d’inscription disponible sur la plateforme. Il doit remplir tous les champs obligatoires de ce formulaire ;
* étape 2 : le diffuseur valide l’inscription en cliquant sur le bouton « Créer mon compte » et accepte les conditions générales en cochant la case prévue à cet effet ;
* étape 3 : la DGE envoie un email confirmant la création du compte et invite le diffuseur à modifier son mot de passe dans une interface dédiée ;
* étape 4 : une fois le mot de passe modifié, le diffuseur est automatiquement redirigé vers le formulaire d’authentification permettant d’accéder à la plateforme.

26\. L’ensemble des fonctionnalités de la plateforme ne sont offertes au diffuseur qu’après validation du compte par l’administrateur.

27\. Le diffuseur a la possibilité de modifier son mot de passe à tout moment au sein de son espace personnel par l’intermédiaire de l’onglet « Administration », puis, « Modifier mon mot de passe ».

28\. Le diffuseur est entièrement responsable de la conservation et de l’utilisation de son mot de passe. Il doit prendre toutes les mesures nécessaires pour empêcher une utilisation non autorisée ou frauduleuse de son compte.

29\. A cet effet, il lui est recommandé :

* de modifier régulièrement son mot de passe ;
* de ne pas utiliser un mot de passe déjà utilisé pour un autre service.

30\. Le diffuseur doit se déconnecter de sa session et fermer la fenêtre de son navigateur à l’issue de ses démarches sur son compte pour éviter que d‘autres personnes n’accèdent à son espace personnel. Le diffuseur doit être particulièrement prudent lorsqu’il accède à son compte en wifi ou partage de connexion.

31\. Si le diffuseur constate ou suspecte une utilisation non autorisée ou frauduleuse de son mot de passe ou toute brèche dans la sécurité, il doit alerter immédiatement la DGE selon les moyens suivants :

* par formulaire de contact auprès de la DGE, disponible à l’adresse https://www.entreprises.gouv.fr/dge/ecrire-a-la-dge?type=DataTourisme
* en utilisant le système de messagerie interne à la plateforme.

32\. A compter de la réception de cette notification, l’administrateur procédera dans un délai raisonnable à la désactivation du compte du diffuseur.

33\. Le mot de passe est strictement personnel et :

* doit correspondre aux besoins de sécurisation fixés par la DGE ;
* ne doit, sous aucun prétexte, être communiqué à un tiers.

34\. **IL EST EXPRESSEMENT RAPPELE QUE LA DGE NE DEMANDE JAMAIS ET POUR QUELQUE RAISON QUE CE SOIT AU DIFFUSEUR DE LUI COMMUNIQUER SON MOT DE PASSE ET QUE TOUTE DEMANDE EN CE SENS DEVRA ETRE CONSIDEREE COMME UNE DEMANDE FRAUDULEUSE.**

35\. Le compte personnel se verrouille après 5 tentatives de connexion infructueuses pendant un délai d’une heure. Le déverrouillage est également possible par l’intermédiaire des administrateurs.

36\. La DGE se réserve le droit de modifier les conditions techniques liées à l’authentification et de substituer au mot de passe toute autre technologie qu’elle estimera nécessaire.

### 10. Notifications

37\. Le diffuseur peut être amené à recevoir, lors de l’utilisation de la plateforme, des messages, sous forme de notifications.

38\. Ces notifications ont vocation à informer le diffuseur sur l’ensemble des événements importants concernant son activité et les flux qu’il manipule dans la plateforme.

### 11. Sécurité

39\. La DGE fait ses meilleurs efforts, conformément aux règles de l’art, pour sécuriser la plateforme et respecte les référentiels de sécurité RGS 2.0, OWASP Top 10 – 2013 ainsi que les bonnes pratiques formulées par l’ANSSI. De plus, tous les échanges entre le navigateur du diffuseur et la plateforme font l’objet d’un chiffrement à l’aide d’un certificat TLS, émanant d’une autorité de certification désignée par la DGE.

40\. Le diffuseur reconnaît avoir connaissance de la nature du réseau de l’internet, et en particulier, de ses performances techniques et des temps de réponse pour consulter, interroger ou transférer les données d’informations.

41\. Le diffuseur se doit d’informer la DGE de toute défaillance ou dysfonctionnement de l’espace diffuseur.

42\. Si une faille dans la sécurité est détectée, la DGE informera le diffuseur dans le respect des dispositions légales qui s’imposent à elle. Elle lui indiquera éventuellement des mesures à prendre. L’exécution de ces mesures est à la charge du diffuseur.

43\. La DGE peut prendre toutes les mesures d’urgence nécessaires à la sécurité de la plateforme.

44\. Le diffuseur accepte de prendre toutes les mesures appropriées de façon à protéger ses propres données et/ou logiciels de la contamination par des éventuels virus ou toute autre technologie nuisible sur le réseau de l’internet.

45\. En tout état de cause, le diffuseur déclare accepter les caractéristiques et limites de l’internet.

### 12. Responsabilité de la DGE

46\. La responsabilité de la DGE ne pourra être recherchée ni retenue en cas d’indisponibilité temporaire ou totale de tout ou partie de l’accès à la plateforme, d’une difficulté liée au temps de réponse, et d’une manière générale, d’un défaut de performance quelconque.

47\. La responsabilité de la DGE ne saurait être recherchée en cas d’usage frauduleux ou abusif du compte dû à une divulgation volontaire ou accidentelle à quiconque de son mot de passe.

48\. La DGE ne saurait être responsable de la violation des présentes conditions par un autre diffuseur.

49\. Compte tenu de l’importance des jeux de données publiés sur la plateforme par des producteurs, la DGE ne saurait être responsable des omissions, inexactitudes ou carences dans leurs mises à jour, qu’elles soient de son fait ou du fait des producteurs qui lui fournissent ces jeux de données, la validation effectuée a priori par la DGE n’ayant vocation qu’à vérifier la compatibilité technique des jeux de données stockées sur la plateforme mais en aucun cas la qualité, l’exactitude ni la véracité des jeux de données stockés sur la plateforme.

50\. Il appartient au diffuseur de vérifier la qualité et la véracité des jeux de données lors de leur réutilisation, cette dernière n’étant réalisée que sous son unique responsabilité.

51\. Le diffuseur reconnaît et accepte que la DGE ne peut pas être tenue pour responsable de la perte, ou altération des jeux de données résultant de l’utilisation non-conforme de la plateforme ou d’une mauvaise gestion de la sécurité par le diffuseur.

### 13. Signalement des contenus

52\. La DGE s’engage dans la lutte contre les contenus illicites, erronés et incompatibles techniquement. A ce titre, la DGE permet au diffuseur de lui notifier toute catégorie de contenus contrevenant aux présentes conditions générales, aux lois et règlements, publiés sur la plateforme.

53\. En plus d’un contrôle a priori réalisé par les administrateurs, la DGE a mis en place un système de contrôle a posteriori de la mise en ligne de jeu de données par les diffuseurs via une fonctionnalité de signalement, permettant d’assurer, dans la mesure du possible, que les jeux de données publiés sur la plateforme respectent les conditions générales de la plateforme.

54\. Cette notification peut être réalisée par n’importe quel diffuseur qui identifierait un contenu illicite, erroné et incompatible techniquement. La notification est engagée sous la seule et unique responsabilité du diffuseur.

55\. Tout jeu de données ayant fait l’objet d’un signalement préalable et qui ne respecterait pas les conditions générales est supprimé par la DGE dans un délai raisonnable à compter du signalement.

### 14. Propriété intellectuelle

56\. Les présentes conditions générales n’emportent aucune cession d’aucune sorte de droits de propriété intellectuelle sur les éléments appartenant à la DGE au bénéfice du diffuseur.

57\. Le site, les marques, les dessins, les modèles, les images, les textes, les photos, les logos, les chartes graphiques, les logiciels, les moteurs de recherche, les structures des bases de données et les noms de domaine, sans que cette liste soit exhaustive, sont la propriété exclusive de la DGE ou de ses partenaires qui lui ont concédé une licence.

58\. Toute reproduction et/ou représentation, totale ou partielle d’un de ces droits, sans l’autorisation expresse de la DGE, est interdite et constituerait une contrefaçon sanctionnée par les articles L. 335-2 et suivants du Code de la propriété intellectuelle.

59\. En conséquence, le diffuseur s'interdit tout agissement et tout acte susceptible de porter atteinte directement ou non aux droits de propriété intellectuelle de la DGE.

60\. La présente clause de propriété intellectuelle à vocation à perdurer après la terminaison des conditions générales.

### 15. Données publiques

61\. Tous les jeux de données publiés sur la plateforme sont régis par la licence ouverte / open licence Etalab à jour au moment de leur publication.

62\. Les producteurs des jeux de données concèdent un droit non exclusif et gratuit de libre extraction et réutilisation des jeux de données publiés sur la plateforme, à des fins commerciales ou non, dans le monde entier et pour une durée illimitée.

63\. Le diffuseur est donc libre de réutiliser les jeux de données afin de :

* les reproduire, les copier ;
* les adapter, les modifier, les extraire et les transformer, pour créer des flux, des produits ou des services ;
* les communiquer, les diffuser, les redistribuer, les publier et les transmettre ;
* les exploiter à titre commercial, par exemple en les combinant avec d’autres jeux de données, ou en les incluant dans son propre produit ou application.

64\. Néanmoins, le diffuseur doit toujours mentionner la paternité du jeu de données utilisé dans le cadre de sa réutilisation et la date de dernière mise à jour du jeu de données réutilisé.

65\. Pour plus de détails sur le régime des jeux de données et sur les droits découlant de ce régime, il convient de se référer à la licence ouverte / open licence Etalab, accessible à l’adresse suivante : https://www.etalab.gouv.fr/wp-content/uploads/2017/04/ETALAB-Licence-Ouverte-v2.0.pdf

### 16. Données à caractère personnel

#### 16.1. A la charge de la DGE

66\. L’accès à la plateforme est conditionné à l’ouverture d’un compte personnel par le diffuseur et nécessite l’utilisation par la DGE de données à caractère personnel le concernant.

67\. Certaines données collectées sont des données à caractère personnel permettant de créer le compte personnel du diffuseur, tels que la société pour laquelle le diffuseur travaille, le nom, prénom et l’adresse e-mail du diffuseur. Le diffuseur peut à tout moment mettre à jour ses données à caractère personnel ou modifier son profil grâce à l’interface « Mon profil ».

68\. Les données sont collectées sur la base du consentement du diffuseur, recueilli lors de l’inscription à la plateforme.

69\. Ces données sont destinées à la DGE, responsable de traitement.

70\. Dans le cadre du bon fonctionnement de la plateforme, la DGE est amenée à recueillir et stocker des données à caractère personnel pour les finalités suivantes :

* la gestion du compte du diffuseur ;
* la gestion des demandes de support technique ;
* la gestion des demandes de droit d’accès, de rectification et d’opposition ;
* la publication de statistiques sur la plateforme et, notamment, sur l’interface utilisable par les producteurs (catégories de données téléchargées par le diffuseur, fréquences des téléchargements etc.).

71\. Les données à caractère personnel sont accessibles aux agents habilités de la DGE dans la limite de leurs fonctions ainsi qu’à des sous-traitants éventuels.

72\. La DGE enregistre, traite les données collectées, à seule fin de permettre au diffuseur le plein et bon usage de la plateforme.

73\. Dans le cadre du partage des données, la DGE peut faire appel à des sous-traitants pour le fonctionnement des services proposés par la plateforme et pour l’hébergement des données.

74\. Les données à caractère personnel du diffuseur sont conservées pendant toute la durée d’activation du compte augmentée des durées de conservation obligatoires en matière de durée légale de prescription.

75\. Le diffuseur est informé que la DGE ne transfère aucune donnée en dehors de France.

76\. Le diffuseur est informé qu'il dispose d'un droit d'accès, d’interrogation et de rectification qui lui permet, le cas échéant, de faire rectifier, compléter, mettre à jour ou effacer les données à caractère personnel le concernant qui sont inexactes, incomplètes, équivoques, périmées ou dont la collecte, l'utilisation, la communication ou la conservation est interdite.

77\. Le diffuseur a également le droit de formuler des directives spécifiques et générales concernant la conservation, l’effacement et la communication de ses données post-mortem. En ce qui concerne les directives générales, elles devront être adressées à tiers qui sera désigné par Décret.

78\. Le diffuseur peut exercer l'ensemble de ces droits en s’adressant à la DGE :

* par courrier à l’adresse : DGE – Bureau de la communication – 67 rue Barbès  - 94200 Ivry sur Seine ;
* par le formulaire de contact électronique disponible à l’adresse : https://www.entreprises.gouv.fr/dge/ecrire-a-la-dge?type=DataTourisme.

79\. Les demandes d’exercice du droit d’accès devront être accompagnées d’une copie d’un titre d’identité en cours de validité.

80\. Enfin, le diffuseur a le droit d’introduire une réclamation auprès de la Commission nationale Informatique et libertés (ci-après « CNIL »).

#### 16.2. A la charge du diffuseur

81\. Les jeux de données publiés sur la plateforme peuvent éventuellement contenir des données à caractère personnel.

82\. Conformément à la licence ouverte / open licence Etalab, le diffuseur est soumis à la règlementation applicable en matière de protection des données à caractère personnel actuellement en vigueur.

83\. A ce titre, le diffuseur garantit la DGE du respect des obligations légales et réglementaires lui incombant au titre de la protection des données à caractère personnel.

84\. Le diffuseur est responsable, vis-à-vis des personnes concernées, des manquements à la règlementation en matière de protection des données à caractère personnel qui pourraient éventuellement lui être reprochés dans le cadre de la réutilisation des jeux de données.

### 17. Statistiques

85\. Afin d’améliorer et de personnaliser la plateforme et son contenu aux besoins des diffuseurs, la DGE conserve l’historique des connexions des diffuseurs, à savoir le nombre de visites effectuées sur la plateforme, le nombre de pages vues, l’activité des diffuseurs sur la plateforme et la fréquence de retour.

86\. Les catégories de données publiées à des fins de statistiques sont déterminées uniquement par la DGE et en aucun cas par un tiers.

### 18. Convention de preuve

87\. Les données récoltées et enregistrées dans les systèmes d’information de la DGE seront conservées dans des conditions raisonnables de sécurité et considérés comme les preuves des communications intervenues entre les parties.

88\. L’acceptation des conditions générales par voie électronique, les notifications au sein de la plateforme ainsi que tout engagement contractuel pris en ligne par le diffuseur ont, entre les parties, la même valeur probante que sur support papier.

89\. La présente disposition doit être considérée comme une convention de preuve valablement admise par les parties.

### 19. Liens hypertextes

90\. La plateforme peut contenir des liens hypertextes donnant accès à des sites web de tiers.

91\. Le diffuseur est formellement informé que les sites web auxquels il peut accéder par l’intermédiaire des liens hypertextes n’appartiennent pas à la DGE.

92\. La DGE décline toute responsabilité quant au contenu des informations fournies sur ces sites au titre de l’activation de l’hyperlien et quant à la politique de confidentialité de ces sites.

93\. Le diffuseur ne peut invoquer la responsabilité de la DGE en cas de perte ou de dommage de quelque sorte que ce soit du fait de l’activation de ces liens hypertextes.

### 20. Cookies

94\. La DGE s’autorise à suivre la navigation du diffuseur sur la plateforme et s’engage a n’utiliser des cookies que soit dans le cadre du suivi de session (nécessaire au bon fonctionnement de la plateforme soit à des fins de mesure d’audience.

### 21. Fermeture du compte

95\. Le diffuseur peut, à tout moment, se désinscrire de la plateforme sur simple demande.

96\. Lorsqu’un diffuseur ne s’est pas connecté ou n’a pas fait usage de son compte pendant un délai de deux ans l’administrateur dispose de la capacité de fermer le compte.

97\. Le diffuseur reçoit un email d’alerte préalablement à toute fermeture de compte.

98\. Toute fermeture est notifiée au diffuseur.

99\. Le diffuseur est informé qu’à partir du moment où il n’est plus utilisateur de la plateforme, il perd l’accès à son espace personnel sécurisé.

100\. La suppression du compte est irréversible.

101\. La fermeture du compte, pour quelque motif que ce soit, implique la perte de l’ensemble des données, informations et avantages conférés par la plateforme.

102\. Il incombe au diffuseur de prendre à sa charge les mesures nécessaires pour conserver ses données s’il le souhaite.

103\. Le diffuseur peut demander la fermeture de son compte auprès de l’administrateur :

* par courrier à l’adresse : DGE – Bureau de la communication – 67 rue Barbès  - 94200 Ivry sur Seine ;
* par le formulaire de contact électronique disponible à l’adresse : https://www.entreprises.gouv.fr/dge/ecrire-a-la-dge?type=DataTourisme
* en utilisant le système de messagerie interne à la plateforme.

### 22. Suspension et résolution

104\. En cas de manquement aux obligations des présentes conditions générales par le diffuseur, l’administrateur se réserve le droit, sans indemnité ni remboursement le cas échéant, huit jours après l’envoi au diffuseur d’un courrier lui demandant de se conformer aux présentes conditions générales, de suspendre l’accès à la plateforme jusqu’à ce que la cause de la suspension ait disparu.

105\. Une suspension de plus de 30 jours non traitée par le diffuseur entraine la suppression du compte.

106\. La DGE pourra fermer le compte de manière immédiate et sans préavis en cas d’usage frauduleux et de mise en cause de sa responsabilité. Toute fermeture du compte est notifiée par email au diffuseur.

107\. La fermeture du compte pour faute du diffuseur lui interdit de rouvrir un espace diffuseur pendant une période d’au moins un an, la réactivation ou la réouverture d’un compte pouvant par ailleurs être en l’espèce conditionnée par un accord express et préalable de la DGE.

### 23. Dispositions générales

108\. Les présentes conditions générales sont régies par la loi française.

109\. En cas de difficultés d’interprétation résultant d’une contradiction entre l’un quelconque des titres figurant en tête des clauses et l’une quelconque des clauses, les titres seront déclarés inexistants.

110\. Si une ou plusieurs stipulations du présent contrat sont tenues pour non valides ou déclarées comme telles en application d’une loi, d’un règlement ou à la suite d’une décision passée en force de chose jugée d’une juridiction compétente, les autres stipulations garderont toute leur force et leur portée.

111\. **TOUT LITIGE CONCERNANT L’ENSEMBLE DES CONDITIONS GENERALES EST DE LA COMPETENCE DES JURIDICTIONS DU RESSORT DE LA COUR D’APPEL DE PARIS, NONOBSTANT PLURALITE DE DEFENDEURS OU APPEL EN GARANTIE.**

