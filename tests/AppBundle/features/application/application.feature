Feature:

  Scenario: Je charge les fixtures.
    Given the database is empty
    And the following fixtures files are loaded:
      | application_type.yml |
      | user_type.yml |
      | user.yml |

  Scenario: En tant qu'admin, je ne peux pas accéder aux applications
    Given I am logged in as "admin@dt.dev" with "admin"
    Then the response status code should be 200
    And I go to "/application"
    Then the response status code should be 403

  Scenario: En tant qu'utilisateur, je ne peux gérer mes applications
    Given I am logged in as "user@dt.dev" with "user"
    Then the response status code should be 200
    Then I clean up the GET globals
    Then I go to "/application"
    Then the response status code should be 200
    Then I should see "Vous n'avez pas encore d'application"
    Then I go to "/application/add"
    And I fill in the following:
      | application[name]         | My application          |
      | application[description]  | Description             |
      | application[url]          | http://www.conjecto.com |
    And I submit the "form" form
    Then the response status code should be 200
    Then I should see "a bien été enregistrée"
    When I go to "/application"
    And I should see "My application"
    Then I follow "Voir la clé"
    And I should see "Cette clé API permet à votre application de consommer vos flux"