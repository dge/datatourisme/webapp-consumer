Feature:

  Scenario: Je charge les fixtures.
    Given the database is empty
    And the following fixtures files are loaded:
      | output_type.yml |
      | application_type.yml |
      | user_type.yml |
      | user.yml |
      | flux.yml |
      | application.yml |
      | process.yml     |
      | download.yml |

  Scenario: L'utilisateur a un flux vide et crée une requête pour la première fois
    Given I am logged in as "certified@dt.dev" with "certified"
    Then I should be on "/flux"
    And the response status code should be 200
#    Then I clean up the GET globals
    When I go to "/flux/65/download/complete"
    Then I submit the "form" form
    And the response status code should be 200
    Then I wait for 15 seconds
    Then I clean up the GET globals
    Then I go to "/flux/65/processes"
    Then I should see "Télécharger la dernière version"
    Then I should see "En attente"
    When I go to "/internal/api/run/65"
    And the response status code should be 200
    Then I go to "/flux/65/editor"
    Then I fill in the following:
      | expert_editor[sparqlRequest]        | CONSTRUCT {?s ?p ?o2} WHERE {?s ?p ?o2} limit 10        |
    Then I submit the "FORM" form