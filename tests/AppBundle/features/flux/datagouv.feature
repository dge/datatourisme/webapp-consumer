Feature: DAT-7 Je peux sélectionner un jeu de donnée pour exporter les données d'un flux

  Scenario: Je charge les fixtures.
    Given the database is empty
    And the following fixtures files are loaded:
      | output_type.yml |
      | user_type.yml |
      | user.yml |
      | flux.yml |

  Scenario: Je dois être certifié pour spécifier un jeu de données
    Given I am logged in as "user@dt.dev" with "user"
    Then the response status code should be 200
    And I go to "/flux/1/parameters"
    Then I should not see "Publier sur data.gouv.fr"

  Scenario: Un message m'indique que je dois renseigner ma clé API avant de pouvoir publier sur datagouv
    Given I am logged in as "certified@dt.dev" with "certified"
    Then the response status code should be 200
    Then I go to "/flux/2/parameters"
    Then the response status code should be 200
    Then I should see "Publier sur data.gouv.fr"
    And I should see "Vous devez renseigner votre clé d'API data.gouv.fr dans votre profil"

  @javascript
  Scenario: Je renseigne ma clé API data.gouv.fr et je  sélectionne un jeu de donnée pour mon flux
    Given I am logged in as "certified@dt.dev" with "certified"
    Then I clean up the GET globals
    When I go to "/account/data-gouv"
    Then I clean up the GET globals
    Then I fill in the data.gouv.fr ApiKey
    And I submit the "form" form
    Then I should see "La clé a bien été enregistrée"
    Then I clean up the GET globals
    Then I go to "/flux/63/parameters"
    Then I fill in the following:
      | flux[dataGouv] | 1 |
    And I submit the "form" form
    Then I should see "Vous devez sélectionner un jeu de données"
    Then I select "select#flux_dataSet":"Jeu de test"
    Then I submit the "form" form
    Then I should see "a bien été mis à jour."
