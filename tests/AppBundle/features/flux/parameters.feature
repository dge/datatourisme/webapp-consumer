Feature:

  Scenario: Je charge les fixtures.
    Given the database is empty
    And the following fixtures files are loaded:
      | output_type.yml |
      | user_type.yml |
      | user.yml |
      | flux.yml |

  Scenario: Je peux éditer un flux
    Given I am logged in as "user@dt.dev" with "user"
    Then I should be on "/flux"
    And the response status code should be 200
    When I go to "/flux/1/parameters"
    And the "flux[outputType]" field should contain "4"
    Then I fill in the following:
      | flux[name]        | "other name"        |
      | flux[description] | "other description" |
      | flux[outputType]  | 10                  |
    Then I submit the "form" form
    Then the response status code should be 200
      Then I wait for 10 seconds
    Then I go to "/flux/1/parameters"
    And the "flux[outputType]" field should contain "10"

  @javascript
  Scenario: La modale d'exemple de format de sortie est dynamique
    Given I am logged in as "user@dt.dev" with "user"
    Then I should be on "/flux"
    When I go to "/flux/1/parameters"
    Then I click the "#outputHelp" element
    Then I wait for 2 seconds
    Then I should see "SQL (exemple)"
    When I go to "/flux/1/parameters"
    Then I select "select#flux_outputType":"CSV"
    Then I click the "#outputHelp" element
    Then I wait for 2 seconds
    Then I should see "CSV (exemple)"

  Scenario: Je peux créer un flux
    Given I am logged in as "user@dt.dev" with "user"
    Then I should be on "/flux"
    And the response status code should be 200
    And I follow "Nouveau flux"
    Then I fill in the following:
      |form[name] | Nouveau flux |
      |form[description] | Nouvelle description |
    Then I submit the "form" form
    Then I should see "Flux : Nouveau flux"
    And the "flux[outputType]" field should contain "4"
