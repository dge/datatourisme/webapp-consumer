Feature: DAT-3 Enregistrement d'un nouveau compte

  Scenario: Je charge les fixtures.
    Given the database is empty

  Scenario:
    Given I am on "/register"
    When I fill in the following:
      | register[organization]  | Conjecto        |
      | register[lastName]      | New             |
      | register[firstName]     | User            |
      | register[email]         | new-user@dt.dev |
    And I submit the "form" form
    Then the response status code should be 200
    Then I should see "Un email vous a été envoyé pour finiliser votre inscription"

    Given I am logged in as "new-user@dt.dev" with "NotSoWeakPassword@1"
    Then I should see "Identifiants invalides."

    Then I check the last received email
    Then the subject should contain "Votre compte vient d'être créé"
    When I follow the first link in the last received email

    Then the response status code should be 200
    When I fill in the following:
      | form[plainPassword][first]  | NotSoWeakPassword@1 |
      | form[plainPassword][second] | NotSoWeakPassword@1 |
    And I submit the "form" form
    Then the response status code should be 200
    And I should be on "/login"
    And I should see "Votre mot de passe a bien été mis à jour."

    When I am logged in as "new-user@dt.dev" with "NotSoWeakPassword@1"
    Then I should be on "/cgu"
    And the response status code should be 200
