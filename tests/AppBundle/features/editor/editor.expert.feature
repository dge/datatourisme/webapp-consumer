Feature: Éditeur SPARQL (mode expert)

  Scenario: Je charge les fixtures.
    Given the database is empty
    And the following fixtures files are loaded:
      | output_type.yml |
      | user_type.yml |
      | user.yml |
      | flux.yml |

  Scenario: En tant qu'utilisateur, je peux accéder à l'éditeur SPARQL (mode expert)
    Given I am logged in as "user@dt.dev" with "user"
    When I go to "/flux/62/editor"
    Then the response status code should be 200
    And I should see "Prévisualiser les résultats"
    And I should see "Enregistrer"

  Scenario: En tant qu'utilisateur, je peux modifier et enregistrer la requête SPARQL (mode expert)
    Given I am logged in as "user@dt.dev" with "user"
    When I go to "/flux/62/editor"
    And I fill in "expert_editor[query]" with "SELECT ?s ?p ?o WHERE { ?s ?p ?o } LIMIT 10"
    And I submit the "form" form
    Then the response status code should be 200
    And I should be on "/flux/62/editor"
    And I should see "SELECT ?s ?p ?o WHERE { ?s ?p ?o } LIMIT 10"
    And I should see "Enregistrer"

  @javascript
  Scenario: En tant qu'utilisateur, je peux prévisualiser un échantillon des résultats de ma requête
    Given I am logged in as "user@dt.dev" with "user"
    And I wait for 2 seconds
    When I go to "/flux/62/editor"
    And I wait for 2 seconds
    When I follow "Prévisualiser les résultats"
    And I wait for 5 seconds
    Then I should see "La prévisualisation des résultats ne contient que les 10 premiers résultats de votre requête."

  Scenario: J'ai des limitations sur ma requête SPARQL
    Given I am logged in as "user@dt.dev" with "user"
    When I go to "/flux/62/editor"
#    Then I fill in "expert_editor[sparqlRequest]" with "PREFIX : <http://example/> PREFIX  dc:     <http://purl.org/dc/elements/1.1/> SELECT ?a FROM <mybooks.rdf> { ?b dc:title ?title . SERVICE <http://sparql.org/books> { ?s dc:title ?title . ?s dc:creator ?a }}"
#    And I submit the "form" form
#    Then the response status code should be 200
#    Then I should see "Le mot clé service ne peut pas être utilisé"
    Then I fill in "expert_editor[query]" with "SELECT ?s ?p ?o WHERE { ?s ?p ?o} ORDER BY ?s"
    And I submit the "form" form
    Then the response status code should be 200
    Then I should see "La requête ne doit pas contenir de directive ORDER BY"
#    Then I fill in "expert_editor[sparqlRequest]" with "CONSTRUCT { ?s ?p ?o } WHERE { ?s ?p ?o } FILTER (?s)"
#    And I submit the "form" form
#    Then the response status code should be 200
#    Then I should see "La requête n'est pas valide"
    Then I fill in "expert_editor[query]" with "SELECT ?s ?p ?o WHERE { ?s ?p ?o} LIMIT 10"
    And I submit the "form" form
    Then the response status code should be 200
    Then I should not see "Éditeur de requête (mode expert)"

  Scenario: En tant qu'utilisateur, je ne peux pas accéder à l'éditeur SPARQL (mode expert) pour un flux qui ne m'appartient pas
    Given I am logged in as "user@dt.dev" with "user"
    When I go to "/flux/2/editor"
    Then the response status code should be 403

  Scenario: En tant qu'administrateur, je ne peux pas accéder à l'éditeur SPARQL (mode expert)
    Given I am logged in as "admin@dt.dev" with "admin"
    When I go to "/flux/2/editor"
    Then the response status code should be 403