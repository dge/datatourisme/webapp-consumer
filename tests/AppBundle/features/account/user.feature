Feature: DAT-6 Profil utilisateur

  Scenario: Je charge les fixtures.
    Given the database is empty
    And the following fixtures files are loaded:
      | user_type.yml |
      | user.yml |

  Scenario: Je peux modifier de mon profil
    Given I am logged in as "user@dt.dev" with "user"
    Then the response status code should be 200
    And I go to "/account"
    When I fill in the following:
      | account[lastName]     | tourisme           |
      | account[firstName]    | Data               |
      | account[zip]          | 35000              |
    And I press "Enregistrer"
    Then I should see "Data TOURISME"
    When I fill in the following:
      | account[lastName]  | DEAU |
      | account[firstName] | Jean |
    And I press "Enregistrer"
    Then I should see "Jean DEAU"
