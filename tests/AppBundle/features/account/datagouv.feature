Feature: DAT-7 Je renseigne ma clé API data.gouv.fr

  Scenario: Je charge les fixtures.
    Given the database is empty
    And the following fixtures files are loaded:
      | user_type.yml |
      | user.yml |

  @javascript
  Scenario: Je renseigne ma clé API data.gouv.fr
    Given I am logged in as "certified@dt.dev" with "certified"
    Then I clean up the GET globals
    When I go to "/account/data-gouv"
    Then I fill in the following:
      | data_gouv_api_key[dataGouvApiKey] | badApiKey |
    And I submit the "form" form
    Then I should see "La clé renseignée est invalide."
    Then I fill in the data.gouv.fr ApiKey
    And I submit the "form" form
    Then I should see "La clé a bien été enregistrée"