Feature: DAT-7 Modification de mot de passe

  Scenario: Je charge les fixtures.
    Given the database is empty
    And the following fixtures files are loaded:
      | user_type.yml |
      | user.yml |

  Scenario: Je peux changer mon mot de passe dans mon profil
    Given I am logged in as "user@dt.dev" with "user"
    Then the response status code should be 200
    When I go to "/account/administrate"
    Then I should see "Modifier mon mot de passe"
    Then I am on "/account/administrate/password"
    Then I fill in the following:
      | update_password[currentPassword]       | user                |
      | update_password[plainPassword][first]  | NotSoWeakPassword@1 |
      | update_password[plainPassword][second] | NotSoWeakPassword@1 |
    Then I submit the "form" form
    Then the response status code should be 200
    Then I disconnect
    Then I am logged in as "user@dt.dev" with "user"
    Then I should see "Identifiants invalides."
    Then I am logged in as "user@dt.dev" with "NotSoWeakPassword@1"
    Then I should not see "Identifiants invalides."